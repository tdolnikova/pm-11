package com.dolnikova.tm;

import com.dolnikova.tm.endpoint.Task;
import org.jetbrains.annotations.Nullable;

import java.util.Comparator;

public class TaskComparator {

    @Nullable
    public static Comparator<Task> getStatusComparator() {
        return new Comparator<Task>() {
            @Override
            public int compare(Task o1, Task o2) {
                return o1.getStatus().compareTo(o2.getStatus());
            }
        };
    }

    @Nullable
    public static Comparator<Task> getCreationDateComparator() {
        return new Comparator<Task>() {
            @Override
            public int compare(Task o1, Task o2) {
                return o1.getCreationDate().toGregorianCalendar().compareTo
                        (o2.getCreationDate().toGregorianCalendar());
            }
        };
    }

    @Nullable
    public static Comparator<Task> getStartDateComparator() {
        return new Comparator<Task>() {
            @Override
            public int compare(Task o1, Task o2) {
                return o1.getStartDate().toGregorianCalendar().compareTo
                        (o2.getStartDate().toGregorianCalendar());
            }
        };
    }

    @Nullable
    public static Comparator<Task> getEndDateComparator() {
        return new Comparator<Task>() {
            @Override
            public int compare(Task o1, Task o2) {
                return o1.getEndDate().toGregorianCalendar().compareTo
                        (o2.getEndDate().toGregorianCalendar());
            }
        };
    }

}
