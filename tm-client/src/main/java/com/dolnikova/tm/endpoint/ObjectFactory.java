
package com.dolnikova.tm.endpoint;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the com.dolnikova.tm.endpoint package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _Exception_QNAME = new QName("http://endpoint.tm.dolnikova.com/", "Exception");
    private final static QName _FindAllByDescriptionUser_QNAME = new QName("http://endpoint.tm.dolnikova.com/", "findAllByDescriptionUser");
    private final static QName _FindAllByDescriptionUserResponse_QNAME = new QName("http://endpoint.tm.dolnikova.com/", "findAllByDescriptionUserResponse");
    private final static QName _FindAllByNameUser_QNAME = new QName("http://endpoint.tm.dolnikova.com/", "findAllByNameUser");
    private final static QName _FindAllByNameUserResponse_QNAME = new QName("http://endpoint.tm.dolnikova.com/", "findAllByNameUserResponse");
    private final static QName _FindAllUser_QNAME = new QName("http://endpoint.tm.dolnikova.com/", "findAllUser");
    private final static QName _FindAllUserResponse_QNAME = new QName("http://endpoint.tm.dolnikova.com/", "findAllUserResponse");
    private final static QName _FindOneByIdUser_QNAME = new QName("http://endpoint.tm.dolnikova.com/", "findOneByIdUser");
    private final static QName _FindOneByIdUserResponse_QNAME = new QName("http://endpoint.tm.dolnikova.com/", "findOneByIdUserResponse");
    private final static QName _FindOneByNameUser_QNAME = new QName("http://endpoint.tm.dolnikova.com/", "findOneByNameUser");
    private final static QName _FindOneByNameUserResponse_QNAME = new QName("http://endpoint.tm.dolnikova.com/", "findOneByNameUserResponse");
    private final static QName _FindOneBySession_QNAME = new QName("http://endpoint.tm.dolnikova.com/", "findOneBySession");
    private final static QName _FindOneBySessionResponse_QNAME = new QName("http://endpoint.tm.dolnikova.com/", "findOneBySessionResponse");
    private final static QName _GetCurrentUserEndpoint_QNAME = new QName("http://endpoint.tm.dolnikova.com/", "getCurrentUserEndpoint");
    private final static QName _GetCurrentUserEndpointResponse_QNAME = new QName("http://endpoint.tm.dolnikova.com/", "getCurrentUserEndpointResponse");
    private final static QName _LoadBinUser_QNAME = new QName("http://endpoint.tm.dolnikova.com/", "loadBinUser");
    private final static QName _LoadBinUserResponse_QNAME = new QName("http://endpoint.tm.dolnikova.com/", "loadBinUserResponse");
    private final static QName _LoadFasterxmlJsonUser_QNAME = new QName("http://endpoint.tm.dolnikova.com/", "loadFasterxmlJsonUser");
    private final static QName _LoadFasterxmlJsonUserResponse_QNAME = new QName("http://endpoint.tm.dolnikova.com/", "loadFasterxmlJsonUserResponse");
    private final static QName _LoadFasterxmlXmlUser_QNAME = new QName("http://endpoint.tm.dolnikova.com/", "loadFasterxmlXmlUser");
    private final static QName _LoadFasterxmlXmlUserResponse_QNAME = new QName("http://endpoint.tm.dolnikova.com/", "loadFasterxmlXmlUserResponse");
    private final static QName _LoadJaxbJsonUser_QNAME = new QName("http://endpoint.tm.dolnikova.com/", "loadJaxbJsonUser");
    private final static QName _LoadJaxbJsonUserResponse_QNAME = new QName("http://endpoint.tm.dolnikova.com/", "loadJaxbJsonUserResponse");
    private final static QName _LoadJaxbXmlUser_QNAME = new QName("http://endpoint.tm.dolnikova.com/", "loadJaxbXmlUser");
    private final static QName _LoadJaxbXmlUserResponse_QNAME = new QName("http://endpoint.tm.dolnikova.com/", "loadJaxbXmlUserResponse");
    private final static QName _MergeUser_QNAME = new QName("http://endpoint.tm.dolnikova.com/", "mergeUser");
    private final static QName _MergeUserResponse_QNAME = new QName("http://endpoint.tm.dolnikova.com/", "mergeUserResponse");
    private final static QName _PersistListUser_QNAME = new QName("http://endpoint.tm.dolnikova.com/", "persistListUser");
    private final static QName _PersistListUserResponse_QNAME = new QName("http://endpoint.tm.dolnikova.com/", "persistListUserResponse");
    private final static QName _PersistUser_QNAME = new QName("http://endpoint.tm.dolnikova.com/", "persistUser");
    private final static QName _PersistUserResponse_QNAME = new QName("http://endpoint.tm.dolnikova.com/", "persistUserResponse");
    private final static QName _RemoveAllUser_QNAME = new QName("http://endpoint.tm.dolnikova.com/", "removeAllUser");
    private final static QName _RemoveAllUserResponse_QNAME = new QName("http://endpoint.tm.dolnikova.com/", "removeAllUserResponse");
    private final static QName _RemoveUser_QNAME = new QName("http://endpoint.tm.dolnikova.com/", "removeUser");
    private final static QName _RemoveUserResponse_QNAME = new QName("http://endpoint.tm.dolnikova.com/", "removeUserResponse");
    private final static QName _SaveBinUser_QNAME = new QName("http://endpoint.tm.dolnikova.com/", "saveBinUser");
    private final static QName _SaveBinUserResponse_QNAME = new QName("http://endpoint.tm.dolnikova.com/", "saveBinUserResponse");
    private final static QName _SaveFasterxmlJsonUser_QNAME = new QName("http://endpoint.tm.dolnikova.com/", "saveFasterxmlJsonUser");
    private final static QName _SaveFasterxmlJsonUserResponse_QNAME = new QName("http://endpoint.tm.dolnikova.com/", "saveFasterxmlJsonUserResponse");
    private final static QName _SaveFasterxmlXmlUser_QNAME = new QName("http://endpoint.tm.dolnikova.com/", "saveFasterxmlXmlUser");
    private final static QName _SaveFasterxmlXmlUserResponse_QNAME = new QName("http://endpoint.tm.dolnikova.com/", "saveFasterxmlXmlUserResponse");
    private final static QName _SaveJaxbJsonUser_QNAME = new QName("http://endpoint.tm.dolnikova.com/", "saveJaxbJsonUser");
    private final static QName _SaveJaxbJsonUserResponse_QNAME = new QName("http://endpoint.tm.dolnikova.com/", "saveJaxbJsonUserResponse");
    private final static QName _SaveJaxbXmlUser_QNAME = new QName("http://endpoint.tm.dolnikova.com/", "saveJaxbXmlUser");
    private final static QName _SaveJaxbXmlUserResponse_QNAME = new QName("http://endpoint.tm.dolnikova.com/", "saveJaxbXmlUserResponse");
    private final static QName _SetCurrentUserEndpoint_QNAME = new QName("http://endpoint.tm.dolnikova.com/", "setCurrentUserEndpoint");
    private final static QName _SetCurrentUserEndpointResponse_QNAME = new QName("http://endpoint.tm.dolnikova.com/", "setCurrentUserEndpointResponse");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.dolnikova.tm.endpoint
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link Exception }
     * 
     */
    public Exception createException() {
        return new Exception();
    }

    /**
     * Create an instance of {@link FindAllByDescriptionUser }
     * 
     */
    public FindAllByDescriptionUser createFindAllByDescriptionUser() {
        return new FindAllByDescriptionUser();
    }

    /**
     * Create an instance of {@link FindAllByDescriptionUserResponse }
     * 
     */
    public FindAllByDescriptionUserResponse createFindAllByDescriptionUserResponse() {
        return new FindAllByDescriptionUserResponse();
    }

    /**
     * Create an instance of {@link FindAllByNameUser }
     * 
     */
    public FindAllByNameUser createFindAllByNameUser() {
        return new FindAllByNameUser();
    }

    /**
     * Create an instance of {@link FindAllByNameUserResponse }
     * 
     */
    public FindAllByNameUserResponse createFindAllByNameUserResponse() {
        return new FindAllByNameUserResponse();
    }

    /**
     * Create an instance of {@link FindAllUser }
     * 
     */
    public FindAllUser createFindAllUser() {
        return new FindAllUser();
    }

    /**
     * Create an instance of {@link FindAllUserResponse }
     * 
     */
    public FindAllUserResponse createFindAllUserResponse() {
        return new FindAllUserResponse();
    }

    /**
     * Create an instance of {@link FindOneByIdUser }
     * 
     */
    public FindOneByIdUser createFindOneByIdUser() {
        return new FindOneByIdUser();
    }

    /**
     * Create an instance of {@link FindOneByIdUserResponse }
     * 
     */
    public FindOneByIdUserResponse createFindOneByIdUserResponse() {
        return new FindOneByIdUserResponse();
    }

    /**
     * Create an instance of {@link FindOneByNameUser }
     * 
     */
    public FindOneByNameUser createFindOneByNameUser() {
        return new FindOneByNameUser();
    }

    /**
     * Create an instance of {@link FindOneByNameUserResponse }
     * 
     */
    public FindOneByNameUserResponse createFindOneByNameUserResponse() {
        return new FindOneByNameUserResponse();
    }

    /**
     * Create an instance of {@link FindOneBySession }
     * 
     */
    public FindOneBySession createFindOneBySession() {
        return new FindOneBySession();
    }

    /**
     * Create an instance of {@link FindOneBySessionResponse }
     * 
     */
    public FindOneBySessionResponse createFindOneBySessionResponse() {
        return new FindOneBySessionResponse();
    }

    /**
     * Create an instance of {@link GetCurrentUserEndpoint }
     * 
     */
    public GetCurrentUserEndpoint createGetCurrentUserEndpoint() {
        return new GetCurrentUserEndpoint();
    }

    /**
     * Create an instance of {@link GetCurrentUserEndpointResponse }
     * 
     */
    public GetCurrentUserEndpointResponse createGetCurrentUserEndpointResponse() {
        return new GetCurrentUserEndpointResponse();
    }

    /**
     * Create an instance of {@link LoadBinUser }
     * 
     */
    public LoadBinUser createLoadBinUser() {
        return new LoadBinUser();
    }

    /**
     * Create an instance of {@link LoadBinUserResponse }
     * 
     */
    public LoadBinUserResponse createLoadBinUserResponse() {
        return new LoadBinUserResponse();
    }

    /**
     * Create an instance of {@link LoadFasterxmlJsonUser }
     * 
     */
    public LoadFasterxmlJsonUser createLoadFasterxmlJsonUser() {
        return new LoadFasterxmlJsonUser();
    }

    /**
     * Create an instance of {@link LoadFasterxmlJsonUserResponse }
     * 
     */
    public LoadFasterxmlJsonUserResponse createLoadFasterxmlJsonUserResponse() {
        return new LoadFasterxmlJsonUserResponse();
    }

    /**
     * Create an instance of {@link LoadFasterxmlXmlUser }
     * 
     */
    public LoadFasterxmlXmlUser createLoadFasterxmlXmlUser() {
        return new LoadFasterxmlXmlUser();
    }

    /**
     * Create an instance of {@link LoadFasterxmlXmlUserResponse }
     * 
     */
    public LoadFasterxmlXmlUserResponse createLoadFasterxmlXmlUserResponse() {
        return new LoadFasterxmlXmlUserResponse();
    }

    /**
     * Create an instance of {@link LoadJaxbJsonUser }
     * 
     */
    public LoadJaxbJsonUser createLoadJaxbJsonUser() {
        return new LoadJaxbJsonUser();
    }

    /**
     * Create an instance of {@link LoadJaxbJsonUserResponse }
     * 
     */
    public LoadJaxbJsonUserResponse createLoadJaxbJsonUserResponse() {
        return new LoadJaxbJsonUserResponse();
    }

    /**
     * Create an instance of {@link LoadJaxbXmlUser }
     * 
     */
    public LoadJaxbXmlUser createLoadJaxbXmlUser() {
        return new LoadJaxbXmlUser();
    }

    /**
     * Create an instance of {@link LoadJaxbXmlUserResponse }
     * 
     */
    public LoadJaxbXmlUserResponse createLoadJaxbXmlUserResponse() {
        return new LoadJaxbXmlUserResponse();
    }

    /**
     * Create an instance of {@link MergeUser }
     * 
     */
    public MergeUser createMergeUser() {
        return new MergeUser();
    }

    /**
     * Create an instance of {@link MergeUserResponse }
     * 
     */
    public MergeUserResponse createMergeUserResponse() {
        return new MergeUserResponse();
    }

    /**
     * Create an instance of {@link PersistListUser }
     * 
     */
    public PersistListUser createPersistListUser() {
        return new PersistListUser();
    }

    /**
     * Create an instance of {@link PersistListUserResponse }
     * 
     */
    public PersistListUserResponse createPersistListUserResponse() {
        return new PersistListUserResponse();
    }

    /**
     * Create an instance of {@link PersistUser }
     * 
     */
    public PersistUser createPersistUser() {
        return new PersistUser();
    }

    /**
     * Create an instance of {@link PersistUserResponse }
     * 
     */
    public PersistUserResponse createPersistUserResponse() {
        return new PersistUserResponse();
    }

    /**
     * Create an instance of {@link RemoveAllUser }
     * 
     */
    public RemoveAllUser createRemoveAllUser() {
        return new RemoveAllUser();
    }

    /**
     * Create an instance of {@link RemoveAllUserResponse }
     * 
     */
    public RemoveAllUserResponse createRemoveAllUserResponse() {
        return new RemoveAllUserResponse();
    }

    /**
     * Create an instance of {@link RemoveUser }
     * 
     */
    public RemoveUser createRemoveUser() {
        return new RemoveUser();
    }

    /**
     * Create an instance of {@link RemoveUserResponse }
     * 
     */
    public RemoveUserResponse createRemoveUserResponse() {
        return new RemoveUserResponse();
    }

    /**
     * Create an instance of {@link SaveBinUser }
     * 
     */
    public SaveBinUser createSaveBinUser() {
        return new SaveBinUser();
    }

    /**
     * Create an instance of {@link SaveBinUserResponse }
     * 
     */
    public SaveBinUserResponse createSaveBinUserResponse() {
        return new SaveBinUserResponse();
    }

    /**
     * Create an instance of {@link SaveFasterxmlJsonUser }
     * 
     */
    public SaveFasterxmlJsonUser createSaveFasterxmlJsonUser() {
        return new SaveFasterxmlJsonUser();
    }

    /**
     * Create an instance of {@link SaveFasterxmlJsonUserResponse }
     * 
     */
    public SaveFasterxmlJsonUserResponse createSaveFasterxmlJsonUserResponse() {
        return new SaveFasterxmlJsonUserResponse();
    }

    /**
     * Create an instance of {@link SaveFasterxmlXmlUser }
     * 
     */
    public SaveFasterxmlXmlUser createSaveFasterxmlXmlUser() {
        return new SaveFasterxmlXmlUser();
    }

    /**
     * Create an instance of {@link SaveFasterxmlXmlUserResponse }
     * 
     */
    public SaveFasterxmlXmlUserResponse createSaveFasterxmlXmlUserResponse() {
        return new SaveFasterxmlXmlUserResponse();
    }

    /**
     * Create an instance of {@link SaveJaxbJsonUser }
     * 
     */
    public SaveJaxbJsonUser createSaveJaxbJsonUser() {
        return new SaveJaxbJsonUser();
    }

    /**
     * Create an instance of {@link SaveJaxbJsonUserResponse }
     * 
     */
    public SaveJaxbJsonUserResponse createSaveJaxbJsonUserResponse() {
        return new SaveJaxbJsonUserResponse();
    }

    /**
     * Create an instance of {@link SaveJaxbXmlUser }
     * 
     */
    public SaveJaxbXmlUser createSaveJaxbXmlUser() {
        return new SaveJaxbXmlUser();
    }

    /**
     * Create an instance of {@link SaveJaxbXmlUserResponse }
     * 
     */
    public SaveJaxbXmlUserResponse createSaveJaxbXmlUserResponse() {
        return new SaveJaxbXmlUserResponse();
    }

    /**
     * Create an instance of {@link SetCurrentUserEndpoint }
     * 
     */
    public SetCurrentUserEndpoint createSetCurrentUserEndpoint() {
        return new SetCurrentUserEndpoint();
    }

    /**
     * Create an instance of {@link SetCurrentUserEndpointResponse }
     * 
     */
    public SetCurrentUserEndpointResponse createSetCurrentUserEndpointResponse() {
        return new SetCurrentUserEndpointResponse();
    }

    /**
     * Create an instance of {@link Session }
     * 
     */
    public Session createSession() {
        return new Session();
    }

    /**
     * Create an instance of {@link User }
     * 
     */
    public User createUser() {
        return new User();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Exception }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.dolnikova.com/", name = "Exception")
    public JAXBElement<Exception> createException(Exception value) {
        return new JAXBElement<Exception>(_Exception_QNAME, Exception.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FindAllByDescriptionUser }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.dolnikova.com/", name = "findAllByDescriptionUser")
    public JAXBElement<FindAllByDescriptionUser> createFindAllByDescriptionUser(FindAllByDescriptionUser value) {
        return new JAXBElement<FindAllByDescriptionUser>(_FindAllByDescriptionUser_QNAME, FindAllByDescriptionUser.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FindAllByDescriptionUserResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.dolnikova.com/", name = "findAllByDescriptionUserResponse")
    public JAXBElement<FindAllByDescriptionUserResponse> createFindAllByDescriptionUserResponse(FindAllByDescriptionUserResponse value) {
        return new JAXBElement<FindAllByDescriptionUserResponse>(_FindAllByDescriptionUserResponse_QNAME, FindAllByDescriptionUserResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FindAllByNameUser }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.dolnikova.com/", name = "findAllByNameUser")
    public JAXBElement<FindAllByNameUser> createFindAllByNameUser(FindAllByNameUser value) {
        return new JAXBElement<FindAllByNameUser>(_FindAllByNameUser_QNAME, FindAllByNameUser.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FindAllByNameUserResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.dolnikova.com/", name = "findAllByNameUserResponse")
    public JAXBElement<FindAllByNameUserResponse> createFindAllByNameUserResponse(FindAllByNameUserResponse value) {
        return new JAXBElement<FindAllByNameUserResponse>(_FindAllByNameUserResponse_QNAME, FindAllByNameUserResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FindAllUser }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.dolnikova.com/", name = "findAllUser")
    public JAXBElement<FindAllUser> createFindAllUser(FindAllUser value) {
        return new JAXBElement<FindAllUser>(_FindAllUser_QNAME, FindAllUser.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FindAllUserResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.dolnikova.com/", name = "findAllUserResponse")
    public JAXBElement<FindAllUserResponse> createFindAllUserResponse(FindAllUserResponse value) {
        return new JAXBElement<FindAllUserResponse>(_FindAllUserResponse_QNAME, FindAllUserResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FindOneByIdUser }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.dolnikova.com/", name = "findOneByIdUser")
    public JAXBElement<FindOneByIdUser> createFindOneByIdUser(FindOneByIdUser value) {
        return new JAXBElement<FindOneByIdUser>(_FindOneByIdUser_QNAME, FindOneByIdUser.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FindOneByIdUserResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.dolnikova.com/", name = "findOneByIdUserResponse")
    public JAXBElement<FindOneByIdUserResponse> createFindOneByIdUserResponse(FindOneByIdUserResponse value) {
        return new JAXBElement<FindOneByIdUserResponse>(_FindOneByIdUserResponse_QNAME, FindOneByIdUserResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FindOneByNameUser }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.dolnikova.com/", name = "findOneByNameUser")
    public JAXBElement<FindOneByNameUser> createFindOneByNameUser(FindOneByNameUser value) {
        return new JAXBElement<FindOneByNameUser>(_FindOneByNameUser_QNAME, FindOneByNameUser.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FindOneByNameUserResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.dolnikova.com/", name = "findOneByNameUserResponse")
    public JAXBElement<FindOneByNameUserResponse> createFindOneByNameUserResponse(FindOneByNameUserResponse value) {
        return new JAXBElement<FindOneByNameUserResponse>(_FindOneByNameUserResponse_QNAME, FindOneByNameUserResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FindOneBySession }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.dolnikova.com/", name = "findOneBySession")
    public JAXBElement<FindOneBySession> createFindOneBySession(FindOneBySession value) {
        return new JAXBElement<FindOneBySession>(_FindOneBySession_QNAME, FindOneBySession.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FindOneBySessionResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.dolnikova.com/", name = "findOneBySessionResponse")
    public JAXBElement<FindOneBySessionResponse> createFindOneBySessionResponse(FindOneBySessionResponse value) {
        return new JAXBElement<FindOneBySessionResponse>(_FindOneBySessionResponse_QNAME, FindOneBySessionResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetCurrentUserEndpoint }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.dolnikova.com/", name = "getCurrentUserEndpoint")
    public JAXBElement<GetCurrentUserEndpoint> createGetCurrentUserEndpoint(GetCurrentUserEndpoint value) {
        return new JAXBElement<GetCurrentUserEndpoint>(_GetCurrentUserEndpoint_QNAME, GetCurrentUserEndpoint.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetCurrentUserEndpointResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.dolnikova.com/", name = "getCurrentUserEndpointResponse")
    public JAXBElement<GetCurrentUserEndpointResponse> createGetCurrentUserEndpointResponse(GetCurrentUserEndpointResponse value) {
        return new JAXBElement<GetCurrentUserEndpointResponse>(_GetCurrentUserEndpointResponse_QNAME, GetCurrentUserEndpointResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link LoadBinUser }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.dolnikova.com/", name = "loadBinUser")
    public JAXBElement<LoadBinUser> createLoadBinUser(LoadBinUser value) {
        return new JAXBElement<LoadBinUser>(_LoadBinUser_QNAME, LoadBinUser.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link LoadBinUserResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.dolnikova.com/", name = "loadBinUserResponse")
    public JAXBElement<LoadBinUserResponse> createLoadBinUserResponse(LoadBinUserResponse value) {
        return new JAXBElement<LoadBinUserResponse>(_LoadBinUserResponse_QNAME, LoadBinUserResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link LoadFasterxmlJsonUser }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.dolnikova.com/", name = "loadFasterxmlJsonUser")
    public JAXBElement<LoadFasterxmlJsonUser> createLoadFasterxmlJsonUser(LoadFasterxmlJsonUser value) {
        return new JAXBElement<LoadFasterxmlJsonUser>(_LoadFasterxmlJsonUser_QNAME, LoadFasterxmlJsonUser.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link LoadFasterxmlJsonUserResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.dolnikova.com/", name = "loadFasterxmlJsonUserResponse")
    public JAXBElement<LoadFasterxmlJsonUserResponse> createLoadFasterxmlJsonUserResponse(LoadFasterxmlJsonUserResponse value) {
        return new JAXBElement<LoadFasterxmlJsonUserResponse>(_LoadFasterxmlJsonUserResponse_QNAME, LoadFasterxmlJsonUserResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link LoadFasterxmlXmlUser }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.dolnikova.com/", name = "loadFasterxmlXmlUser")
    public JAXBElement<LoadFasterxmlXmlUser> createLoadFasterxmlXmlUser(LoadFasterxmlXmlUser value) {
        return new JAXBElement<LoadFasterxmlXmlUser>(_LoadFasterxmlXmlUser_QNAME, LoadFasterxmlXmlUser.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link LoadFasterxmlXmlUserResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.dolnikova.com/", name = "loadFasterxmlXmlUserResponse")
    public JAXBElement<LoadFasterxmlXmlUserResponse> createLoadFasterxmlXmlUserResponse(LoadFasterxmlXmlUserResponse value) {
        return new JAXBElement<LoadFasterxmlXmlUserResponse>(_LoadFasterxmlXmlUserResponse_QNAME, LoadFasterxmlXmlUserResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link LoadJaxbJsonUser }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.dolnikova.com/", name = "loadJaxbJsonUser")
    public JAXBElement<LoadJaxbJsonUser> createLoadJaxbJsonUser(LoadJaxbJsonUser value) {
        return new JAXBElement<LoadJaxbJsonUser>(_LoadJaxbJsonUser_QNAME, LoadJaxbJsonUser.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link LoadJaxbJsonUserResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.dolnikova.com/", name = "loadJaxbJsonUserResponse")
    public JAXBElement<LoadJaxbJsonUserResponse> createLoadJaxbJsonUserResponse(LoadJaxbJsonUserResponse value) {
        return new JAXBElement<LoadJaxbJsonUserResponse>(_LoadJaxbJsonUserResponse_QNAME, LoadJaxbJsonUserResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link LoadJaxbXmlUser }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.dolnikova.com/", name = "loadJaxbXmlUser")
    public JAXBElement<LoadJaxbXmlUser> createLoadJaxbXmlUser(LoadJaxbXmlUser value) {
        return new JAXBElement<LoadJaxbXmlUser>(_LoadJaxbXmlUser_QNAME, LoadJaxbXmlUser.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link LoadJaxbXmlUserResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.dolnikova.com/", name = "loadJaxbXmlUserResponse")
    public JAXBElement<LoadJaxbXmlUserResponse> createLoadJaxbXmlUserResponse(LoadJaxbXmlUserResponse value) {
        return new JAXBElement<LoadJaxbXmlUserResponse>(_LoadJaxbXmlUserResponse_QNAME, LoadJaxbXmlUserResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link MergeUser }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.dolnikova.com/", name = "mergeUser")
    public JAXBElement<MergeUser> createMergeUser(MergeUser value) {
        return new JAXBElement<MergeUser>(_MergeUser_QNAME, MergeUser.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link MergeUserResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.dolnikova.com/", name = "mergeUserResponse")
    public JAXBElement<MergeUserResponse> createMergeUserResponse(MergeUserResponse value) {
        return new JAXBElement<MergeUserResponse>(_MergeUserResponse_QNAME, MergeUserResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link PersistListUser }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.dolnikova.com/", name = "persistListUser")
    public JAXBElement<PersistListUser> createPersistListUser(PersistListUser value) {
        return new JAXBElement<PersistListUser>(_PersistListUser_QNAME, PersistListUser.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link PersistListUserResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.dolnikova.com/", name = "persistListUserResponse")
    public JAXBElement<PersistListUserResponse> createPersistListUserResponse(PersistListUserResponse value) {
        return new JAXBElement<PersistListUserResponse>(_PersistListUserResponse_QNAME, PersistListUserResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link PersistUser }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.dolnikova.com/", name = "persistUser")
    public JAXBElement<PersistUser> createPersistUser(PersistUser value) {
        return new JAXBElement<PersistUser>(_PersistUser_QNAME, PersistUser.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link PersistUserResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.dolnikova.com/", name = "persistUserResponse")
    public JAXBElement<PersistUserResponse> createPersistUserResponse(PersistUserResponse value) {
        return new JAXBElement<PersistUserResponse>(_PersistUserResponse_QNAME, PersistUserResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RemoveAllUser }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.dolnikova.com/", name = "removeAllUser")
    public JAXBElement<RemoveAllUser> createRemoveAllUser(RemoveAllUser value) {
        return new JAXBElement<RemoveAllUser>(_RemoveAllUser_QNAME, RemoveAllUser.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RemoveAllUserResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.dolnikova.com/", name = "removeAllUserResponse")
    public JAXBElement<RemoveAllUserResponse> createRemoveAllUserResponse(RemoveAllUserResponse value) {
        return new JAXBElement<RemoveAllUserResponse>(_RemoveAllUserResponse_QNAME, RemoveAllUserResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RemoveUser }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.dolnikova.com/", name = "removeUser")
    public JAXBElement<RemoveUser> createRemoveUser(RemoveUser value) {
        return new JAXBElement<RemoveUser>(_RemoveUser_QNAME, RemoveUser.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RemoveUserResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.dolnikova.com/", name = "removeUserResponse")
    public JAXBElement<RemoveUserResponse> createRemoveUserResponse(RemoveUserResponse value) {
        return new JAXBElement<RemoveUserResponse>(_RemoveUserResponse_QNAME, RemoveUserResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SaveBinUser }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.dolnikova.com/", name = "saveBinUser")
    public JAXBElement<SaveBinUser> createSaveBinUser(SaveBinUser value) {
        return new JAXBElement<SaveBinUser>(_SaveBinUser_QNAME, SaveBinUser.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SaveBinUserResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.dolnikova.com/", name = "saveBinUserResponse")
    public JAXBElement<SaveBinUserResponse> createSaveBinUserResponse(SaveBinUserResponse value) {
        return new JAXBElement<SaveBinUserResponse>(_SaveBinUserResponse_QNAME, SaveBinUserResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SaveFasterxmlJsonUser }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.dolnikova.com/", name = "saveFasterxmlJsonUser")
    public JAXBElement<SaveFasterxmlJsonUser> createSaveFasterxmlJsonUser(SaveFasterxmlJsonUser value) {
        return new JAXBElement<SaveFasterxmlJsonUser>(_SaveFasterxmlJsonUser_QNAME, SaveFasterxmlJsonUser.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SaveFasterxmlJsonUserResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.dolnikova.com/", name = "saveFasterxmlJsonUserResponse")
    public JAXBElement<SaveFasterxmlJsonUserResponse> createSaveFasterxmlJsonUserResponse(SaveFasterxmlJsonUserResponse value) {
        return new JAXBElement<SaveFasterxmlJsonUserResponse>(_SaveFasterxmlJsonUserResponse_QNAME, SaveFasterxmlJsonUserResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SaveFasterxmlXmlUser }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.dolnikova.com/", name = "saveFasterxmlXmlUser")
    public JAXBElement<SaveFasterxmlXmlUser> createSaveFasterxmlXmlUser(SaveFasterxmlXmlUser value) {
        return new JAXBElement<SaveFasterxmlXmlUser>(_SaveFasterxmlXmlUser_QNAME, SaveFasterxmlXmlUser.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SaveFasterxmlXmlUserResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.dolnikova.com/", name = "saveFasterxmlXmlUserResponse")
    public JAXBElement<SaveFasterxmlXmlUserResponse> createSaveFasterxmlXmlUserResponse(SaveFasterxmlXmlUserResponse value) {
        return new JAXBElement<SaveFasterxmlXmlUserResponse>(_SaveFasterxmlXmlUserResponse_QNAME, SaveFasterxmlXmlUserResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SaveJaxbJsonUser }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.dolnikova.com/", name = "saveJaxbJsonUser")
    public JAXBElement<SaveJaxbJsonUser> createSaveJaxbJsonUser(SaveJaxbJsonUser value) {
        return new JAXBElement<SaveJaxbJsonUser>(_SaveJaxbJsonUser_QNAME, SaveJaxbJsonUser.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SaveJaxbJsonUserResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.dolnikova.com/", name = "saveJaxbJsonUserResponse")
    public JAXBElement<SaveJaxbJsonUserResponse> createSaveJaxbJsonUserResponse(SaveJaxbJsonUserResponse value) {
        return new JAXBElement<SaveJaxbJsonUserResponse>(_SaveJaxbJsonUserResponse_QNAME, SaveJaxbJsonUserResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SaveJaxbXmlUser }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.dolnikova.com/", name = "saveJaxbXmlUser")
    public JAXBElement<SaveJaxbXmlUser> createSaveJaxbXmlUser(SaveJaxbXmlUser value) {
        return new JAXBElement<SaveJaxbXmlUser>(_SaveJaxbXmlUser_QNAME, SaveJaxbXmlUser.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SaveJaxbXmlUserResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.dolnikova.com/", name = "saveJaxbXmlUserResponse")
    public JAXBElement<SaveJaxbXmlUserResponse> createSaveJaxbXmlUserResponse(SaveJaxbXmlUserResponse value) {
        return new JAXBElement<SaveJaxbXmlUserResponse>(_SaveJaxbXmlUserResponse_QNAME, SaveJaxbXmlUserResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SetCurrentUserEndpoint }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.dolnikova.com/", name = "setCurrentUserEndpoint")
    public JAXBElement<SetCurrentUserEndpoint> createSetCurrentUserEndpoint(SetCurrentUserEndpoint value) {
        return new JAXBElement<SetCurrentUserEndpoint>(_SetCurrentUserEndpoint_QNAME, SetCurrentUserEndpoint.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SetCurrentUserEndpointResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.dolnikova.com/", name = "setCurrentUserEndpointResponse")
    public JAXBElement<SetCurrentUserEndpointResponse> createSetCurrentUserEndpointResponse(SetCurrentUserEndpointResponse value) {
        return new JAXBElement<SetCurrentUserEndpointResponse>(_SetCurrentUserEndpointResponse_QNAME, SetCurrentUserEndpointResponse.class, null, value);
    }

}
