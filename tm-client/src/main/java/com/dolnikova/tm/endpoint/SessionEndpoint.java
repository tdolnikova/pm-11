package com.dolnikova.tm.endpoint;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.ws.Action;
import javax.xml.ws.FaultAction;
import javax.xml.ws.RequestWrapper;
import javax.xml.ws.ResponseWrapper;

/**
 * This class was generated by Apache CXF 3.2.7
 * 2019-11-13T12:48:38.566+03:00
 * Generated source version: 3.2.7
 *
 */
@WebService(targetNamespace = "http://endpoint.tm.dolnikova.com/", name = "SessionEndpoint")
@XmlSeeAlso({ObjectFactory.class})
public interface SessionEndpoint {

    @WebMethod
    @Action(input = "http://endpoint.tm.dolnikova.com/SessionEndpoint/loadFasterxmlJsonSessionRequest", output = "http://endpoint.tm.dolnikova.com/SessionEndpoint/loadFasterxmlJsonSessionResponse", fault = {@FaultAction(className = Exception_Exception.class, value = "http://endpoint.tm.dolnikova.com/SessionEndpoint/loadFasterxmlJsonSession/Fault/Exception")})
    @RequestWrapper(localName = "loadFasterxmlJsonSession", targetNamespace = "http://endpoint.tm.dolnikova.com/", className = "com.dolnikova.tm.endpoint.LoadFasterxmlJsonSession")
    @ResponseWrapper(localName = "loadFasterxmlJsonSessionResponse", targetNamespace = "http://endpoint.tm.dolnikova.com/", className = "com.dolnikova.tm.endpoint.LoadFasterxmlJsonSessionResponse")
    @WebResult(name = "return", targetNamespace = "")
    public java.util.List<com.dolnikova.tm.endpoint.Session> loadFasterxmlJsonSession(
        @WebParam(name = "session", targetNamespace = "")
        com.dolnikova.tm.endpoint.Session session,
        @WebParam(name = "user", targetNamespace = "")
        com.dolnikova.tm.endpoint.User user
    ) throws Exception_Exception;

    @WebMethod
    @Action(input = "http://endpoint.tm.dolnikova.com/SessionEndpoint/loadJaxbXmlSessionRequest", output = "http://endpoint.tm.dolnikova.com/SessionEndpoint/loadJaxbXmlSessionResponse", fault = {@FaultAction(className = Exception_Exception.class, value = "http://endpoint.tm.dolnikova.com/SessionEndpoint/loadJaxbXmlSession/Fault/Exception")})
    @RequestWrapper(localName = "loadJaxbXmlSession", targetNamespace = "http://endpoint.tm.dolnikova.com/", className = "com.dolnikova.tm.endpoint.LoadJaxbXmlSession")
    @ResponseWrapper(localName = "loadJaxbXmlSessionResponse", targetNamespace = "http://endpoint.tm.dolnikova.com/", className = "com.dolnikova.tm.endpoint.LoadJaxbXmlSessionResponse")
    @WebResult(name = "return", targetNamespace = "")
    public java.util.List<com.dolnikova.tm.endpoint.Session> loadJaxbXmlSession(
        @WebParam(name = "session", targetNamespace = "")
        com.dolnikova.tm.endpoint.Session session,
        @WebParam(name = "user", targetNamespace = "")
        com.dolnikova.tm.endpoint.User user
    ) throws Exception_Exception;

    @WebMethod
    @Action(input = "http://endpoint.tm.dolnikova.com/SessionEndpoint/saveJaxbXmlSessionRequest", output = "http://endpoint.tm.dolnikova.com/SessionEndpoint/saveJaxbXmlSessionResponse", fault = {@FaultAction(className = Exception_Exception.class, value = "http://endpoint.tm.dolnikova.com/SessionEndpoint/saveJaxbXmlSession/Fault/Exception")})
    @RequestWrapper(localName = "saveJaxbXmlSession", targetNamespace = "http://endpoint.tm.dolnikova.com/", className = "com.dolnikova.tm.endpoint.SaveJaxbXmlSession")
    @ResponseWrapper(localName = "saveJaxbXmlSessionResponse", targetNamespace = "http://endpoint.tm.dolnikova.com/", className = "com.dolnikova.tm.endpoint.SaveJaxbXmlSessionResponse")
    @WebResult(name = "return", targetNamespace = "")
    public boolean saveJaxbXmlSession(
        @WebParam(name = "session", targetNamespace = "")
        com.dolnikova.tm.endpoint.Session session,
        @WebParam(name = "user", targetNamespace = "")
        com.dolnikova.tm.endpoint.User user,
        @WebParam(name = "sessions", targetNamespace = "")
        java.util.List<com.dolnikova.tm.endpoint.Session> sessions
    ) throws Exception_Exception;

    @WebMethod
    @Action(input = "http://endpoint.tm.dolnikova.com/SessionEndpoint/getSessionRequest", output = "http://endpoint.tm.dolnikova.com/SessionEndpoint/getSessionResponse")
    @RequestWrapper(localName = "getSession", targetNamespace = "http://endpoint.tm.dolnikova.com/", className = "com.dolnikova.tm.endpoint.GetSession")
    @ResponseWrapper(localName = "getSessionResponse", targetNamespace = "http://endpoint.tm.dolnikova.com/", className = "com.dolnikova.tm.endpoint.GetSessionResponse")
    @WebResult(name = "return", targetNamespace = "")
    public com.dolnikova.tm.endpoint.Session getSession(
        @WebParam(name = "login", targetNamespace = "")
        java.lang.String login,
        @WebParam(name = "password", targetNamespace = "")
        java.lang.String password
    );

    @WebMethod
    @Action(input = "http://endpoint.tm.dolnikova.com/SessionEndpoint/saveJaxbJsonSessionRequest", output = "http://endpoint.tm.dolnikova.com/SessionEndpoint/saveJaxbJsonSessionResponse", fault = {@FaultAction(className = Exception_Exception.class, value = "http://endpoint.tm.dolnikova.com/SessionEndpoint/saveJaxbJsonSession/Fault/Exception")})
    @RequestWrapper(localName = "saveJaxbJsonSession", targetNamespace = "http://endpoint.tm.dolnikova.com/", className = "com.dolnikova.tm.endpoint.SaveJaxbJsonSession")
    @ResponseWrapper(localName = "saveJaxbJsonSessionResponse", targetNamespace = "http://endpoint.tm.dolnikova.com/", className = "com.dolnikova.tm.endpoint.SaveJaxbJsonSessionResponse")
    @WebResult(name = "return", targetNamespace = "")
    public boolean saveJaxbJsonSession(
        @WebParam(name = "session", targetNamespace = "")
        com.dolnikova.tm.endpoint.Session session,
        @WebParam(name = "user", targetNamespace = "")
        com.dolnikova.tm.endpoint.User user,
        @WebParam(name = "sessions", targetNamespace = "")
        java.util.List<com.dolnikova.tm.endpoint.Session> sessions
    ) throws Exception_Exception;

    @WebMethod
    @Action(input = "http://endpoint.tm.dolnikova.com/SessionEndpoint/loadJaxbJsonSessionRequest", output = "http://endpoint.tm.dolnikova.com/SessionEndpoint/loadJaxbJsonSessionResponse", fault = {@FaultAction(className = Exception_Exception.class, value = "http://endpoint.tm.dolnikova.com/SessionEndpoint/loadJaxbJsonSession/Fault/Exception")})
    @RequestWrapper(localName = "loadJaxbJsonSession", targetNamespace = "http://endpoint.tm.dolnikova.com/", className = "com.dolnikova.tm.endpoint.LoadJaxbJsonSession")
    @ResponseWrapper(localName = "loadJaxbJsonSessionResponse", targetNamespace = "http://endpoint.tm.dolnikova.com/", className = "com.dolnikova.tm.endpoint.LoadJaxbJsonSessionResponse")
    @WebResult(name = "return", targetNamespace = "")
    public java.util.List<com.dolnikova.tm.endpoint.Session> loadJaxbJsonSession(
        @WebParam(name = "session", targetNamespace = "")
        com.dolnikova.tm.endpoint.Session session,
        @WebParam(name = "user", targetNamespace = "")
        com.dolnikova.tm.endpoint.User user
    ) throws Exception_Exception;

    @WebMethod
    @Action(input = "http://endpoint.tm.dolnikova.com/SessionEndpoint/saveFasterxmlXmlSessionRequest", output = "http://endpoint.tm.dolnikova.com/SessionEndpoint/saveFasterxmlXmlSessionResponse", fault = {@FaultAction(className = Exception_Exception.class, value = "http://endpoint.tm.dolnikova.com/SessionEndpoint/saveFasterxmlXmlSession/Fault/Exception")})
    @RequestWrapper(localName = "saveFasterxmlXmlSession", targetNamespace = "http://endpoint.tm.dolnikova.com/", className = "com.dolnikova.tm.endpoint.SaveFasterxmlXmlSession")
    @ResponseWrapper(localName = "saveFasterxmlXmlSessionResponse", targetNamespace = "http://endpoint.tm.dolnikova.com/", className = "com.dolnikova.tm.endpoint.SaveFasterxmlXmlSessionResponse")
    @WebResult(name = "return", targetNamespace = "")
    public boolean saveFasterxmlXmlSession(
        @WebParam(name = "session", targetNamespace = "")
        com.dolnikova.tm.endpoint.Session session,
        @WebParam(name = "user", targetNamespace = "")
        com.dolnikova.tm.endpoint.User user,
        @WebParam(name = "sessions", targetNamespace = "")
        java.util.List<com.dolnikova.tm.endpoint.Session> sessions
    ) throws Exception_Exception;

    @WebMethod
    @Action(input = "http://endpoint.tm.dolnikova.com/SessionEndpoint/loadBinSessionRequest", output = "http://endpoint.tm.dolnikova.com/SessionEndpoint/loadBinSessionResponse", fault = {@FaultAction(className = Exception_Exception.class, value = "http://endpoint.tm.dolnikova.com/SessionEndpoint/loadBinSession/Fault/Exception")})
    @RequestWrapper(localName = "loadBinSession", targetNamespace = "http://endpoint.tm.dolnikova.com/", className = "com.dolnikova.tm.endpoint.LoadBinSession")
    @ResponseWrapper(localName = "loadBinSessionResponse", targetNamespace = "http://endpoint.tm.dolnikova.com/", className = "com.dolnikova.tm.endpoint.LoadBinSessionResponse")
    @WebResult(name = "return", targetNamespace = "")
    public java.util.List<com.dolnikova.tm.endpoint.Session> loadBinSession(
        @WebParam(name = "session", targetNamespace = "")
        com.dolnikova.tm.endpoint.Session session,
        @WebParam(name = "user", targetNamespace = "")
        com.dolnikova.tm.endpoint.User user
    ) throws Exception_Exception;

    @WebMethod
    @Action(input = "http://endpoint.tm.dolnikova.com/SessionEndpoint/loadFasterxmlXmlSessionRequest", output = "http://endpoint.tm.dolnikova.com/SessionEndpoint/loadFasterxmlXmlSessionResponse", fault = {@FaultAction(className = Exception_Exception.class, value = "http://endpoint.tm.dolnikova.com/SessionEndpoint/loadFasterxmlXmlSession/Fault/Exception")})
    @RequestWrapper(localName = "loadFasterxmlXmlSession", targetNamespace = "http://endpoint.tm.dolnikova.com/", className = "com.dolnikova.tm.endpoint.LoadFasterxmlXmlSession")
    @ResponseWrapper(localName = "loadFasterxmlXmlSessionResponse", targetNamespace = "http://endpoint.tm.dolnikova.com/", className = "com.dolnikova.tm.endpoint.LoadFasterxmlXmlSessionResponse")
    @WebResult(name = "return", targetNamespace = "")
    public java.util.List<com.dolnikova.tm.endpoint.Session> loadFasterxmlXmlSession(
        @WebParam(name = "session", targetNamespace = "")
        com.dolnikova.tm.endpoint.Session session,
        @WebParam(name = "user", targetNamespace = "")
        com.dolnikova.tm.endpoint.User user
    ) throws Exception_Exception;

    @WebMethod
    @Action(input = "http://endpoint.tm.dolnikova.com/SessionEndpoint/saveFasterxmlJsonSessionRequest", output = "http://endpoint.tm.dolnikova.com/SessionEndpoint/saveFasterxmlJsonSessionResponse", fault = {@FaultAction(className = Exception_Exception.class, value = "http://endpoint.tm.dolnikova.com/SessionEndpoint/saveFasterxmlJsonSession/Fault/Exception")})
    @RequestWrapper(localName = "saveFasterxmlJsonSession", targetNamespace = "http://endpoint.tm.dolnikova.com/", className = "com.dolnikova.tm.endpoint.SaveFasterxmlJsonSession")
    @ResponseWrapper(localName = "saveFasterxmlJsonSessionResponse", targetNamespace = "http://endpoint.tm.dolnikova.com/", className = "com.dolnikova.tm.endpoint.SaveFasterxmlJsonSessionResponse")
    @WebResult(name = "return", targetNamespace = "")
    public boolean saveFasterxmlJsonSession(
        @WebParam(name = "session", targetNamespace = "")
        com.dolnikova.tm.endpoint.Session session,
        @WebParam(name = "user", targetNamespace = "")
        com.dolnikova.tm.endpoint.User user,
        @WebParam(name = "sessions", targetNamespace = "")
        java.util.List<com.dolnikova.tm.endpoint.Session> sessions
    ) throws Exception_Exception;

    @WebMethod
    @Action(input = "http://endpoint.tm.dolnikova.com/SessionEndpoint/saveBinSessionRequest", output = "http://endpoint.tm.dolnikova.com/SessionEndpoint/saveBinSessionResponse", fault = {@FaultAction(className = Exception_Exception.class, value = "http://endpoint.tm.dolnikova.com/SessionEndpoint/saveBinSession/Fault/Exception")})
    @RequestWrapper(localName = "saveBinSession", targetNamespace = "http://endpoint.tm.dolnikova.com/", className = "com.dolnikova.tm.endpoint.SaveBinSession")
    @ResponseWrapper(localName = "saveBinSessionResponse", targetNamespace = "http://endpoint.tm.dolnikova.com/", className = "com.dolnikova.tm.endpoint.SaveBinSessionResponse")
    @WebResult(name = "return", targetNamespace = "")
    public boolean saveBinSession(
        @WebParam(name = "session", targetNamespace = "")
        com.dolnikova.tm.endpoint.Session session,
        @WebParam(name = "user", targetNamespace = "")
        com.dolnikova.tm.endpoint.User user,
        @WebParam(name = "sessions", targetNamespace = "")
        java.util.List<com.dolnikova.tm.endpoint.Session> sessions
    ) throws Exception_Exception;
}
