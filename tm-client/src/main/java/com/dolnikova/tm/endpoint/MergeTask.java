
package com.dolnikova.tm.endpoint;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for mergeTask complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="mergeTask"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="session" type="{http://endpoint.tm.dolnikova.com/}session" minOccurs="0"/&gt;
 *         &lt;element name="ownerId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="newData" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="entityToMerge" type="{http://endpoint.tm.dolnikova.com/}task" minOccurs="0"/&gt;
 *         &lt;element name="dataType" type="{http://endpoint.tm.dolnikova.com/}dataType" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "mergeTask", propOrder = {
    "session",
    "ownerId",
    "newData",
    "entityToMerge",
    "dataType"
})
public class MergeTask {

    protected Session session;
    protected String ownerId;
    protected String newData;
    protected Task entityToMerge;
    @XmlSchemaType(name = "string")
    protected DataType dataType;

    /**
     * Gets the value of the session property.
     * 
     * @return
     *     possible object is
     *     {@link Session }
     *     
     */
    public Session getSession() {
        return session;
    }

    /**
     * Sets the value of the session property.
     * 
     * @param value
     *     allowed object is
     *     {@link Session }
     *     
     */
    public void setSession(Session value) {
        this.session = value;
    }

    /**
     * Gets the value of the ownerId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOwnerId() {
        return ownerId;
    }

    /**
     * Sets the value of the ownerId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOwnerId(String value) {
        this.ownerId = value;
    }

    /**
     * Gets the value of the newData property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNewData() {
        return newData;
    }

    /**
     * Sets the value of the newData property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNewData(String value) {
        this.newData = value;
    }

    /**
     * Gets the value of the entityToMerge property.
     * 
     * @return
     *     possible object is
     *     {@link Task }
     *     
     */
    public Task getEntityToMerge() {
        return entityToMerge;
    }

    /**
     * Sets the value of the entityToMerge property.
     * 
     * @param value
     *     allowed object is
     *     {@link Task }
     *     
     */
    public void setEntityToMerge(Task value) {
        this.entityToMerge = value;
    }

    /**
     * Gets the value of the dataType property.
     * 
     * @return
     *     possible object is
     *     {@link DataType }
     *     
     */
    public DataType getDataType() {
        return dataType;
    }

    /**
     * Sets the value of the dataType property.
     * 
     * @param value
     *     allowed object is
     *     {@link DataType }
     *     
     */
    public void setDataType(DataType value) {
        this.dataType = value;
    }

}
