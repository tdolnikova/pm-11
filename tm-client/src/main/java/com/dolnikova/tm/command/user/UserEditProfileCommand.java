package com.dolnikova.tm.command.user;

import com.dolnikova.tm.bootstrap.Bootstrap;
import com.dolnikova.tm.command.AbstractCommand;
import com.dolnikova.tm.constant.AdditionalMessage;
import com.dolnikova.tm.constant.Command;
import com.dolnikova.tm.endpoint.DataType;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public final class UserEditProfileCommand extends AbstractCommand {

    @NotNull
    @Override
    public String command() {
        return Command.USER_EDIT_PROFILE;
    }

    @NotNull
    @Override
    public String description() {
        return Command.USER_EDIT_PROFILE_DESCRIPTION;
    }

    @Override
    public void execute() throws Exception {
        if (!isSecure()) return;
        System.out.println(AdditionalMessage.ENTER_FIELD_NAME + "\n" + DataType.NAME);
        @NotNull String fieldToEdit = "";
        while (fieldToEdit.isEmpty()) {
            fieldToEdit = Bootstrap.scanner.nextLine();
        }
        if (fieldToEdit.equals(DataType.NAME.toString())) {
            changeData(DataType.NAME);
            System.out.println(AdditionalMessage.LOGIN_CHANGED);
        }
    }

    private void changeData(@Nullable DataType dataType) {
        System.out.println(AdditionalMessage.ENTER + dataType.toString());
        @NotNull String newData = "";
        while (newData.isEmpty()) {
            newData = Bootstrap.scanner.nextLine();
        }
        serviceLocator.getUserEndpoint().mergeUser(
                serviceLocator.getSession(),
                serviceLocator.getUserEndpoint().getCurrentUserEndpoint().getId(),
                newData,
                serviceLocator.getUserEndpoint().getCurrentUserEndpoint(),
                dataType);
    }

    @Override
    public boolean isSecure() {
        return (!(serviceLocator.getUserEndpoint().getCurrentUserEndpoint() == null));
    }
}
