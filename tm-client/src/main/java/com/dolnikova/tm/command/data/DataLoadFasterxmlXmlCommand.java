package com.dolnikova.tm.command.data;

import com.dolnikova.tm.command.AbstractCommand;
import com.dolnikova.tm.constant.Command;
import com.dolnikova.tm.endpoint.Project;
import com.dolnikova.tm.endpoint.Task;
import com.dolnikova.tm.endpoint.User;
import org.jetbrains.annotations.NotNull;

import java.util.List;

public final class DataLoadFasterxmlXmlCommand extends AbstractCommand {

    @Override
    public @NotNull String command() {
        return Command.DATA_LOAD_FASTERXML_XML;
    }

    @Override
    public @NotNull String description() {
        return Command.DATA_LOAD_FASTERXML_XML_DESCRIPTION;
    }

    @Override
    public void execute() throws Exception {
        List<Project> projects = serviceLocator.getProjectEndpoint().loadFasterxmlXmlProject(serviceLocator.getSession(), serviceLocator.getUser());
        List<Task> tasks = serviceLocator.getTaskEndpoint().loadFasterxmlXmlTask(serviceLocator.getSession(), serviceLocator.getUser());
        List<User> users = serviceLocator.getUserEndpoint().loadFasterxmlXmlUser(serviceLocator.getSession(), serviceLocator.getUser());
        if (projects == null
                || tasks == null
                || users == null) {
            throw new IllegalAccessException();
        }
        System.out.println("ЗАГРУЖЕНО");
    }

    @Override
    public boolean isSecure() {
        return (!(serviceLocator.getUserEndpoint().getCurrentUserEndpoint() == null));
    }

}