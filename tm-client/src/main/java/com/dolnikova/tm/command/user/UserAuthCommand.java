package com.dolnikova.tm.command.user;

import com.dolnikova.tm.bootstrap.Bootstrap;
import com.dolnikova.tm.command.AbstractCommand;
import com.dolnikova.tm.constant.AdditionalMessage;
import com.dolnikova.tm.constant.Command;
import com.dolnikova.tm.endpoint.Session;
import com.dolnikova.tm.endpoint.User;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public final class UserAuthCommand extends AbstractCommand {

    @NotNull
    @Override
    public String command() {
        return Command.USER_AUTH;
    }

    @NotNull
    @Override
    public String description() {
        return Command.USER_AUTH_DESCRIPTION;
    }

    @Override
    public void execute() throws Exception {
        if (!isSecure()) return;
        System.out.println("[AUTHORIZATION]");
        System.out.println(AdditionalMessage.REG_ENTER_LOGIN);
        @NotNull String name = "";
        while (name.isEmpty()) {
            name = Bootstrap.scanner.nextLine();
        }
        System.out.println(AdditionalMessage.REG_ENTER_PASSWORD);
        @NotNull String description = "";
        while (description.isEmpty()) {
            description = Bootstrap.scanner.nextLine();
        }
        @Nullable final Session session = serviceLocator.getSessionEndpoint().getSession(name, description);
        @Nullable final User currentUser = serviceLocator.getUserEndpoint().findOneBySession(session);
        serviceLocator.setSession(session);
        serviceLocator.setUser(currentUser);
        serviceLocator.getUserEndpoint().setCurrentUserEndpoint(session, currentUser);
        System.out.println(AdditionalMessage.REG_USER_SUCCESSFUL_LOGIN);
    }

    @Override
    public boolean isSecure() {
        return serviceLocator.getUserEndpoint().getCurrentUserEndpoint() == null;
    }
}
