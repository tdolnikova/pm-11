package com.dolnikova.tm.command.task;

import com.dolnikova.tm.bootstrap.Bootstrap;
import com.dolnikova.tm.command.AbstractCommand;
import com.dolnikova.tm.constant.AdditionalMessage;
import com.dolnikova.tm.constant.Command;
import com.dolnikova.tm.endpoint.Project;
import com.dolnikova.tm.endpoint.Task;
import com.dolnikova.tm.util.DateUtil;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public final class TaskPersistCommand extends AbstractCommand {

    @NotNull
    @Override
    public String command() {
        return Command.PERSIST_TASK;
    }

    @NotNull
    @Override
    public String description() {
        return Command.PERSIST_TASK_DESCRIPTION;
    }

    @Override
    public void execute() throws Exception {
        if (!isSecure()) return;
        @Nullable final Project project = findProject();
        if (project == null) return;
        System.out.println(AdditionalMessage.INSERT_TASK);
        boolean taskCreationCompleted = false;
        while (!taskCreationCompleted) {
            @NotNull final String taskText = Bootstrap.scanner.nextLine();
            if (taskText.isEmpty()) taskCreationCompleted = true;
            else {
                @Nullable final Task newTask = new Task();
                newTask.setOwnerId(serviceLocator.getUserEndpoint().getCurrentUserEndpoint().getId());
                newTask.setName(taskText);
                newTask.setProjectId(project.getId());
                System.out.println(AdditionalMessage.INSERT_START_DATE);
                boolean dateChosen = false;
                while (!dateChosen) {
                    @NotNull String startDate = Bootstrap.scanner.nextLine();
                    if (startDate.isEmpty()) return;
                    newTask.setStartDate(DateUtil.stringToXMLGregorianCalendar(startDate));
                    dateChosen = true;
                }
                System.out.println(AdditionalMessage.INSERT_END_DATE);
                dateChosen = false;
                while (!dateChosen) {
                    @NotNull final String endDate = Bootstrap.scanner.nextLine();
                    if (endDate.isEmpty()) return;
                    newTask.setEndDate(DateUtil.stringToXMLGregorianCalendar(endDate));
                    dateChosen = true;
                }
                serviceLocator.getTaskEndpoint().persistTask(
                        serviceLocator.getSession(),
                        serviceLocator.getUserEndpoint().getCurrentUserEndpoint().getId(),
                        newTask);
                System.out.println(AdditionalMessage.TASK + " " + taskText + " " + AdditionalMessage.CREATED_F);
            }
        }
        System.out.println(AdditionalMessage.TASK_ADDITION_COMPLETED);
    }

    @Nullable
    private Project findProject() {
        if (serviceLocator.getProjectEndpoint().findAllProject(
                serviceLocator.getSession(),
                serviceLocator.getUserEndpoint().getCurrentUserEndpoint().getId()).isEmpty()) {
            System.out.println(AdditionalMessage.NO_PROJECTS);
            return null;
        }
        System.out.println(AdditionalMessage.CHOOSE_PROJECT);
        @Nullable Project project = null;
        while (project == null) {
            @NotNull final String projectName = Bootstrap.scanner.nextLine();
            if (projectName.isEmpty()) break;
            project = serviceLocator.getProjectEndpoint().findOneByIdProject(
                    serviceLocator.getSession(),
                    serviceLocator.getUserEndpoint().getCurrentUserEndpoint().getId(),
                    projectName);
            if (project == null) System.out.println(AdditionalMessage.PROJECT_NAME_DOESNT_EXIST + " " + AdditionalMessage.TRY_AGAIN);
        }
        return project;
    }

    @Override
    public boolean isSecure() {
        return (!(serviceLocator.getUserEndpoint().getCurrentUserEndpoint() == null));
    }
}
