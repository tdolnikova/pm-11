package com.dolnikova.tm.command.project;

import com.dolnikova.tm.ProjectComparator;
import com.dolnikova.tm.bootstrap.Bootstrap;
import com.dolnikova.tm.command.AbstractCommand;
import com.dolnikova.tm.constant.AdditionalMessage;
import com.dolnikova.tm.constant.Command;
import com.dolnikova.tm.endpoint.Project;
import com.dolnikova.tm.enumerated.SortingType;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Collections;
import java.util.List;

public final class ProjectSortListCommand extends AbstractCommand {

    @NotNull
    @Override
    public String command() {
        return Command.SORT_PROJECTS;
    }

    @NotNull
    @Override
    public String description() {
        return Command.SORT_PROJECTS_DESCRIPTION;
    }

    @Override
    public void execute() throws Exception {
        if (!isSecure()) return;
        @Nullable final List<Project> projectList = serviceLocator.getProjectEndpoint().findAllProject(
                serviceLocator.getSession(),
                serviceLocator.getUserEndpoint().getCurrentUserEndpoint().getId());
        if (projectList == null || projectList.isEmpty()) {
            System.out.println(AdditionalMessage.NO_PROJECTS);
            return;
        }
        System.out.println(AdditionalMessage.CHOOSE_SORTING_TYPE + ":");
        for(final SortingType type : SortingType.values()) {
            System.out.println(type.displayName());
        }
        boolean typeChosen = false;
        while (!typeChosen) {
            @NotNull final String userChoice = Bootstrap.scanner.nextLine();
            if (userChoice.isEmpty()) return;
            if (userChoice.equals(SortingType.BY_STATUS.displayName())){
                typeChosen = true;
                Collections.sort(projectList, ProjectComparator.getStatusComparator());
            }
            else if (userChoice.equals(SortingType.BY_CREATION_DATE.displayName())){
                typeChosen = true;
                Collections.sort(projectList, ProjectComparator.getCreationDateComparator());
            }
            else if (userChoice.equals(SortingType.BY_START_DATE.displayName())){
                typeChosen = true;
                Collections.sort(projectList, ProjectComparator.getStartDateComparator());
            }
            else if (userChoice.equals(SortingType.BY_END_DATE.displayName())){
                typeChosen = true;
                Collections.sort(projectList, ProjectComparator.getEndDateComparator());
            }
            else {
                System.out.println(AdditionalMessage.TRY_AGAIN);
            }
        }
        for (final Project project : projectList) {
            System.out.println(project.getId());
        }
    }

    @Override
    public boolean isSecure() {
        return (!(serviceLocator.getUserEndpoint().getCurrentUserEndpoint() == null));
    }
}
