package com.dolnikova.tm.command.project;

import com.dolnikova.tm.bootstrap.Bootstrap;
import com.dolnikova.tm.command.AbstractCommand;
import com.dolnikova.tm.constant.AdditionalMessage;
import com.dolnikova.tm.constant.Command;
import com.dolnikova.tm.endpoint.Project;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.List;

public final class ProjectFindByDescriptionCommand extends AbstractCommand {

    @NotNull
    @Override
    public String command() {
        return Command.FIND_PROJECT_BY_DESCRIPTION;
    }

    @NotNull
    @Override
    public String description() {
        return Command.FIND_PROJECT_BY_DESCRIPTION_DESCRIPTION;
    }

    @Override
    public void execute() throws Exception {
        if (!isSecure()) return;
        @NotNull String userInput = "";
        System.out.println(AdditionalMessage.INSERT_TEXT);
        while (userInput.isEmpty()) {
            userInput = Bootstrap.scanner.nextLine();
        }
        @Nullable final List<Project> projectList = serviceLocator.getProjectEndpoint().findAllByDescriptionProject(
                serviceLocator.getSession(),
                serviceLocator.getUserEndpoint().getCurrentUserEndpoint().getId(),
                userInput);
        for (final Project project : projectList) {
            System.out.println(project.getName());
        }
    }

    @Override
    public boolean isSecure() {
        return (!(serviceLocator.getUserEndpoint().getCurrentUserEndpoint() == null));
    }
}
