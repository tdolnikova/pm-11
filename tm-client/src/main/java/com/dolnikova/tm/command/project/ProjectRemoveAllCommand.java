package com.dolnikova.tm.command.project;

import com.dolnikova.tm.command.AbstractCommand;
import com.dolnikova.tm.constant.Command;
import org.jetbrains.annotations.NotNull;

public final class ProjectRemoveAllCommand extends AbstractCommand {

    @NotNull
    @Override
    public String command() {
        return Command.REMOVE_ALL_PROJECTS;
    }

    @NotNull
    @Override
    public String description() {
        return Command.REMOVE_ALL_PROJECTS_DESCRIPTION;
    }

    @Override
    public void execute() throws Exception {
        if (!isSecure()) return;
        serviceLocator.getProjectEndpoint().removeAllProject(
                serviceLocator.getSession(),
                serviceLocator.getUserEndpoint().getCurrentUserEndpoint().getId());
    }

    @Override
    public boolean isSecure() {
        return (!(serviceLocator.getUserEndpoint().getCurrentUserEndpoint() == null));
    }
}
