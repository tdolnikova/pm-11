package com.dolnikova.tm.command.task;

import com.dolnikova.tm.bootstrap.Bootstrap;
import com.dolnikova.tm.command.AbstractCommand;
import com.dolnikova.tm.constant.AdditionalMessage;
import com.dolnikova.tm.constant.Command;
import com.dolnikova.tm.endpoint.Project;
import com.dolnikova.tm.endpoint.Task;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.ArrayList;
import java.util.List;

public final class TaskRemoveCommand extends AbstractCommand {

    @NotNull
    @Override
    public String command() {
        return Command.REMOVE_TASK;
    }

    @NotNull
    @Override
    public String description() {
        return Command.REMOVE_TASK_DESCRIPTION;
    }

    @Override
    public void execute() throws Exception {
        if (!isSecure()) return;
        @Nullable final Project project = findProject();
        if (project == null) return;
        @Nullable final List<Task> tasks = serviceLocator.getTaskEndpoint().findAllTask(
                serviceLocator.getSession(),
                serviceLocator.getUserEndpoint().getCurrentUserEndpoint().getId());
        if (tasks == null || tasks.isEmpty()) {
            System.out.println(AdditionalMessage.NO_TASKS);
            return;
        }
        @Nullable final List<Task> projectTasks = new ArrayList<>();
        for (Task task : tasks) {
            if (task.getProjectId().equals(project.getId())
                    && task.getOwnerId().equals(serviceLocator.getUserEndpoint().getCurrentUserEndpoint().getId()))
                projectTasks.add(task);
        }
        if (projectTasks.isEmpty()) {
            System.out.println(AdditionalMessage.NO_TASKS_IN_PROJECT);
            return;
        }
        System.out.println(AdditionalMessage.IN_PROJECT + project.getName() + " " + projectTasks.size() + " " + AdditionalMessage.OF_TASKS_WITH_POINT + ":");
        for (final Task task : projectTasks) {
            System.out.println(task.getId());
        }
        System.out.println(AdditionalMessage.INSERT_TASK_ID);
        boolean taskDeleted = false;
        while (!taskDeleted) {
            @NotNull final String taskIdToDelete = Bootstrap.scanner.nextLine();
            if (taskIdToDelete.isEmpty()) break;
            @Nullable final Task taskToRemove = serviceLocator.getTaskEndpoint().findOneByIdTask(
                    serviceLocator.getSession(),
                    serviceLocator.getUserEndpoint().getCurrentUserEndpoint().getId(),
                    taskIdToDelete);
            serviceLocator.getTaskEndpoint().removeTask(
                    serviceLocator.getSession(),
                    serviceLocator.getUserEndpoint().getCurrentUserEndpoint().getId(),
                    taskToRemove);
            System.out.println(AdditionalMessage.TASK_DELETED);
            taskDeleted = true;
        }
    }

    private Project findProject() {
        if (serviceLocator.getTaskEndpoint().findAllTask(
                serviceLocator.getSession(),
                serviceLocator.getUserEndpoint().getCurrentUserEndpoint().getId()).isEmpty()) {
            System.out.println(AdditionalMessage.NO_TASKS);
            return null;
        }
        if (serviceLocator.getProjectEndpoint().findAllProject(
                serviceLocator.getSession(),
                serviceLocator.getUserEndpoint().getCurrentUserEndpoint().getId()).isEmpty()) {
            System.out.println(AdditionalMessage.NO_PROJECTS);
            return null;
        }
        System.out.println(AdditionalMessage.CHOOSE_PROJECT);
        @Nullable Project project = null;
        while (project == null) {
            @NotNull final String projectName = Bootstrap.scanner.nextLine();
            if (projectName.isEmpty()) break;
            project = serviceLocator.getProjectEndpoint().findOneByIdProject(
                    serviceLocator.getSession(),
                    serviceLocator.getUserEndpoint().getCurrentUserEndpoint().getId(),
                    projectName);
            if (project == null)
                System.out.println(AdditionalMessage.PROJECT_NAME_DOESNT_EXIST + " " + AdditionalMessage.TRY_AGAIN);
        }
        return project;
    }

    @Override
    public boolean isSecure() {
        return (!(serviceLocator.getUserEndpoint().getCurrentUserEndpoint() == null));
    }

}
