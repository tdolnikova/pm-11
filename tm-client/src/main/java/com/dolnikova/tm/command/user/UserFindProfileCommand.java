package com.dolnikova.tm.command.user;

import com.dolnikova.tm.command.AbstractCommand;
import com.dolnikova.tm.constant.Command;
import org.jetbrains.annotations.NotNull;

public final class UserFindProfileCommand extends AbstractCommand {

    @NotNull
    @Override
    public String command() {
        return Command.USER_FIND_PROFILE;
    }

    @NotNull
    @Override
    public String description() {
        return Command.USER_FIND_PROFILE_DESCRIPTION;
    }

    @Override
    public void execute() throws Exception {
        if (!isSecure()) return;
        System.out.println("id: " + serviceLocator.getUserEndpoint().getCurrentUserEndpoint().getId());
        System.out.println("login: " + serviceLocator.getUserEndpoint().getCurrentUserEndpoint().getName());
        System.out.println("password: " + serviceLocator.getUserEndpoint().getCurrentUserEndpoint().getDescription());
        System.out.println("role: " + serviceLocator.getUserEndpoint().getCurrentUserEndpoint().getRole());
    }

    @Override
    public boolean isSecure() {
        return (!(serviceLocator.getUserEndpoint().getCurrentUserEndpoint() == null));
    }

}
