package com.dolnikova.tm.command.exit;

import com.dolnikova.tm.command.AbstractCommand;
import com.dolnikova.tm.constant.Command;
import org.jetbrains.annotations.NotNull;

public final class ExitCommand extends AbstractCommand {

    @NotNull
    @Override
    public String command() {
        return Command.EXIT;
    }

    @NotNull
    @Override
    public String description() {
        return Command.EXIT_DESCRIPTION;
    }

    @Override
    public void execute() throws Exception {
        if (!isSecure()) return;
        System.exit(0);
    }

    @Override
    public boolean isSecure() {
        return true;
    }
}
