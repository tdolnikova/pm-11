package com.dolnikova.tm.command.project;

import com.dolnikova.tm.bootstrap.Bootstrap;
import com.dolnikova.tm.command.AbstractCommand;
import com.dolnikova.tm.constant.AdditionalMessage;
import com.dolnikova.tm.constant.Command;
import com.dolnikova.tm.endpoint.Project;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public final class ProjectRemoveCommand extends AbstractCommand {

    @NotNull
    @Override
    public String command() {
        return Command.REMOVE_PROJECT;
    }

    @NotNull
    @Override
    public String description() {
        return Command.REMOVE_PROJECT_DESCRIPTION;
    }

    @Override
    public void execute() throws Exception {
        if (!isSecure()) return;
        if (serviceLocator.getProjectEndpoint().findAllProject(
                serviceLocator.getSession(),
                serviceLocator.getUserEndpoint().getCurrentUserEndpoint().getId()).isEmpty()) {
            System.out.println(AdditionalMessage.NO_PROJECTS);
            return;
        }
        System.out.println(AdditionalMessage.WHICH_PROJECT_DELETE);
        @Nullable Project project = null;
        while (project == null) {
            @NotNull final String projectName = Bootstrap.scanner.nextLine();
            if (projectName.isEmpty()) return;
            project = serviceLocator.getProjectEndpoint().findOneByIdProject(
                    serviceLocator.getSession(),
                    serviceLocator.getUserEndpoint().getCurrentUserEndpoint().getId(),
                    projectName);
            if (project != null) serviceLocator.getProjectEndpoint().removeProject(
                    serviceLocator.getSession(),
                    serviceLocator.getUserEndpoint().getCurrentUserEndpoint().getId(),
                    project);
            else System.out.println(AdditionalMessage.PROJECT_NAME_DOESNT_EXIST + " " + AdditionalMessage.TRY_AGAIN);
        }
        System.out.println(AdditionalMessage.PROJECT_DELETED);
    }

    @Override
    public boolean isSecure() {
        return (!(serviceLocator.getUserEndpoint().getCurrentUserEndpoint() == null));
    }
}
