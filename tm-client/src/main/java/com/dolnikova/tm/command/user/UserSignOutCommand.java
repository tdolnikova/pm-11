package com.dolnikova.tm.command.user;

import com.dolnikova.tm.command.AbstractCommand;
import com.dolnikova.tm.constant.Command;
import org.jetbrains.annotations.NotNull;

public final class UserSignOutCommand extends AbstractCommand {

    @NotNull
    @Override
    public String command() {
        return Command.USER_SIGN_OUT;
    }

    @NotNull
    @Override
    public String description() {
        return Command.USER_SIGN_OUT_DESCRIPTION;
    }

    @Override
    public void execute() throws Exception {
        if (!isSecure()) return;
        serviceLocator.getUserEndpoint().setCurrentUserEndpoint(serviceLocator.getSession(), null);
    }

    @Override
    public boolean isSecure() {
        return (!(serviceLocator.getUserEndpoint().getCurrentUserEndpoint() == null));
    }
}
