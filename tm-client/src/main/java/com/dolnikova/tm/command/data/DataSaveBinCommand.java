package com.dolnikova.tm.command.data;

import com.dolnikova.tm.command.AbstractCommand;
import com.dolnikova.tm.constant.Command;
import com.dolnikova.tm.endpoint.Project;
import com.dolnikova.tm.endpoint.Task;
import com.dolnikova.tm.endpoint.User;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.List;

public final class DataSaveBinCommand extends AbstractCommand {

    @Override
    public @NotNull String command() {
        return Command.DATA_SAVE_BIN;
    }

    @Override
    public @NotNull String description() {
        return Command.DATA_SAVE_BIN_DESCRIPTION;
    }

    @Override
    public void execute() throws Exception {
        @Nullable final String ownerId = serviceLocator.getUserEndpoint().getCurrentUserEndpoint().getId();
        List<User> users = serviceLocator.getUserEndpoint().findAllUser(serviceLocator.getSession(), ownerId);
        List<Project> projects = serviceLocator.getProjectEndpoint().findAllProject(serviceLocator.getSession(), ownerId);
        List<Task> tasks = serviceLocator.getTaskEndpoint().findAllTask(serviceLocator.getSession(), ownerId);
        boolean userSaveAllowed = serviceLocator.getUserEndpoint().saveBinUser(serviceLocator.getSession(), serviceLocator.getUser(), users);
        if (userSaveAllowed) System.out.println("ПОЛЬЗОВАТЕЛИ СОХРАНЕНЫ");
        boolean taskSaveAllowed = serviceLocator.getTaskEndpoint().saveBinTask(serviceLocator.getSession(), serviceLocator.getUser(), tasks);
        if (taskSaveAllowed) System.out.println("ЗАДАЧИ СОХРАНЕНЫ");
        boolean projectSaveAllowed = serviceLocator.getProjectEndpoint().saveBinProject(serviceLocator.getSession(), serviceLocator.getUser(), projects);
        if (projectSaveAllowed) System.out.println("ПРОЕКТЫ СОХРАНЕНЫ");
    }

    @Override
    public boolean isSecure() {
        return (!(serviceLocator.getUserEndpoint().getCurrentUserEndpoint() == null));
    }

}
