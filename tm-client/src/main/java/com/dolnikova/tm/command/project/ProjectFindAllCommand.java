package com.dolnikova.tm.command.project;

import com.dolnikova.tm.command.AbstractCommand;
import com.dolnikova.tm.constant.AdditionalMessage;
import com.dolnikova.tm.constant.Command;
import com.dolnikova.tm.endpoint.Project;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.List;

public final class ProjectFindAllCommand extends AbstractCommand {

    @NotNull
    @Override
    public String command() {
        return Command.FIND_ALL_PROJECTS;
    }

    @NotNull
    @Override
    public String description() {
        return Command.FIND_ALL_PROJECTS_DESCRIPTION;
    }

    @Override
    public void execute() {
        if (!isSecure()) return;
        System.out.println("[PROJECT LIST]");
        @Nullable final List<Project> allProjects = serviceLocator.getProjectEndpoint().findAllProject(
                serviceLocator.getSession(),
                serviceLocator.getUserEndpoint().getCurrentUserEndpoint().getId());
        if (allProjects.isEmpty()) System.out.println(AdditionalMessage.NO_PROJECTS);
        else {
            for (final Project project : allProjects) {
                System.out.println(project.getId());
            }
        }
    }

    @Override
    public boolean isSecure() {
        return (!(serviceLocator.getUserEndpoint().getCurrentUserEndpoint() == null));
    }
}
