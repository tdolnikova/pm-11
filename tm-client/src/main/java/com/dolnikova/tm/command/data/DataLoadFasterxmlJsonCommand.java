package com.dolnikova.tm.command.data;

import com.dolnikova.tm.command.AbstractCommand;
import com.dolnikova.tm.constant.Command;
import com.dolnikova.tm.endpoint.Project;
import com.dolnikova.tm.endpoint.Task;
import com.dolnikova.tm.endpoint.User;
import com.dolnikova.tm.entity.EntityDTO;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.List;

public final class DataLoadFasterxmlJsonCommand extends AbstractCommand {

    @Override
    public @NotNull String command() {
        return Command.DATA_LOAD_FASTERXML_JSON;
    }

    @Override
    public @NotNull String description() {
        return Command.DATA_LOAD_FASTERXML_JSON_DESCRIPTION;
    }

    @Override
    public void execute() throws Exception {
        List<Project> projects = serviceLocator.getProjectEndpoint().loadFasterxmlJsonProject(serviceLocator.getSession(), serviceLocator.getUser());
        List<Task> tasks = serviceLocator.getTaskEndpoint().loadFasterxmlJsonTask(serviceLocator.getSession(), serviceLocator.getUser());
        List<User> users = serviceLocator.getUserEndpoint().loadFasterxmlJsonUser(serviceLocator.getSession(), serviceLocator.getUser());
        if (projects == null
                || tasks == null
                || users == null) {
            throw new IllegalAccessException();
        }
        System.out.println("ЗАГРУЖЕНО");

    }

    @Override
    public boolean isSecure() {
        return (!(serviceLocator.getUserEndpoint().getCurrentUserEndpoint() == null));
    }
}
