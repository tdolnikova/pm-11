package com.dolnikova.tm.command.user;


import com.dolnikova.tm.bootstrap.Bootstrap;
import com.dolnikova.tm.command.AbstractCommand;
import com.dolnikova.tm.constant.AdditionalMessage;

import com.dolnikova.tm.constant.Command;
import com.dolnikova.tm.endpoint.Role;
import com.dolnikova.tm.endpoint.User;
import org.jetbrains.annotations.NotNull;

public final class UserRegisterCommand extends AbstractCommand {

    @NotNull
    @Override
    public String command() {
        return Command.USER_REG;
    }

    @NotNull
    @Override
    public String description() {
        return Command.USER_REG_DESCRIPTION;
    }

    @Override
    public void execute() throws Exception {
        if (!isSecure()) return;
        System.out.println("[REGISTRATION]");
        System.out.println(AdditionalMessage.REG_ENTER_LOGIN);
        @NotNull String login = "";
        while (login.isEmpty()) {
            login = Bootstrap.scanner.nextLine();
        }
        System.out.println(AdditionalMessage.REG_ENTER_PASSWORD);
        @NotNull String password = "";
        while (password.isEmpty()) {
            password = Bootstrap.scanner.nextLine();
        }
        if (serviceLocator.getUserEndpoint().findOneByNameUser(login) != null) {
            System.out.println(AdditionalMessage.REG_USER_EXISTS);
            return;
        }
        @NotNull final User newUser = new User();
        newUser.setName(login);
        newUser.setDescription(password);
        newUser.setRole(Role.USER);
        serviceLocator.getUserEndpoint().persistUser(newUser);
        System.out.println(AdditionalMessage.REG_USER_REGISTERED);
    }

    @Override
    public boolean isSecure() {
        return serviceLocator.getUserEndpoint().getCurrentUserEndpoint() == null;
    }
}
