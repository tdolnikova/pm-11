package com.dolnikova.tm.command.task;

import com.dolnikova.tm.command.AbstractCommand;
import com.dolnikova.tm.constant.AdditionalMessage;
import com.dolnikova.tm.constant.Command;
import com.dolnikova.tm.endpoint.Task;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.List;

public final class TaskFindAllCommand extends AbstractCommand {

    @NotNull
    @Override
    public String command() {
        return Command.FIND_ALL_TASKS;
    }

    @NotNull
    @Override
    public String description() {
        return Command.FIND_ALL_TASKS_DESCRIPTION;
    }

    @Override
    public void execute() throws Exception {
        if(!isSecure()) return;
        @Nullable final List<Task> allTasks = serviceLocator.getTaskEndpoint().findAllTask(
                serviceLocator.getSession(),
                serviceLocator.getUserEndpoint().getCurrentUserEndpoint().getId());
        if (allTasks.isEmpty()) System.out.println(AdditionalMessage.NO_TASKS);
        else {
            for (final Task task : allTasks) {
                System.out.println(task.getName());
            }
        }
    }

    @Override
    public boolean isSecure() {
        return (!(serviceLocator.getUserEndpoint().getCurrentUserEndpoint() == null));
    }
}
