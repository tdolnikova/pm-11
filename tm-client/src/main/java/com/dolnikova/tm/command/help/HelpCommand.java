package com.dolnikova.tm.command.help;

import com.dolnikova.tm.command.AbstractCommand;
import com.dolnikova.tm.constant.Command;
import org.jetbrains.annotations.NotNull;

public final class HelpCommand extends AbstractCommand {

    @NotNull
    @Override
    public String command() { return Command.HELP; }

    @NotNull
    @Override
    public String description() { return Command.HELP_DESCRIPTION; }

    @Override
    public void execute() {
        if (!isSecure()) return;
        for (final AbstractCommand command: serviceLocator.getCommands()) {
            System.out.println(command.command() + ": " + command.description());
        }
    }

    @Override
    public boolean isSecure() {
        return true;
    }

}
