package com.dolnikova.tm.command.task;

import com.dolnikova.tm.bootstrap.Bootstrap;
import com.dolnikova.tm.command.AbstractCommand;
import com.dolnikova.tm.constant.AdditionalMessage;
import com.dolnikova.tm.constant.Command;
import com.dolnikova.tm.endpoint.DataType;
import com.dolnikova.tm.endpoint.Project;
import com.dolnikova.tm.endpoint.Task;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.ArrayList;
import java.util.List;

public final class TaskMergeCommand extends AbstractCommand {

    @NotNull
    @Override
    public String command() {
        return Command.MERGE_TASK;
    }

    @NotNull
    @Override
    public String description() {
        return Command.MERGE_TASK_DESCRIPTION;
    }

    @Override
    public void execute() throws Exception {
        if (!isSecure()) return;
        @Nullable final Project project = findProject();
        if (project == null) return;
        @Nullable final List<Task> tasks = serviceLocator.getTaskEndpoint().findAllTask(
                serviceLocator.getSession(),
                serviceLocator.getUserEndpoint().getCurrentUserEndpoint().getId());
        if (tasks == null || tasks.isEmpty()) {
            System.out.println(AdditionalMessage.NO_TASKS);
            return;
        }
        @Nullable final List<Task> projectTasks = new ArrayList<>();
        for (Task task : tasks) {
            if (task.getOwnerId().equals(project.getId()))
                projectTasks.add(task);
        }
        if (projectTasks.isEmpty()) {
            System.out.println(AdditionalMessage.NO_TASKS_IN_PROJECT);
            return;
        }
        System.out.println(AdditionalMessage.IN_PROJECT + project.getName() + " " + projectTasks.size() + " " + AdditionalMessage.OF_TASKS_WITH_POINT + ":");
        for (final Task task : projectTasks) {
            System.out.println(task.getId());
        }
        System.out.println(AdditionalMessage.INSERT_TASK_ID);
        boolean taskUpdated = false;
        while (!taskUpdated) {
            @NotNull final String taskIdToUpdate = Bootstrap.scanner.nextLine();
            if (taskIdToUpdate.isEmpty()) break;
            @Nullable final Task taskToUpdate = serviceLocator.getTaskEndpoint().findOneByIdTask(
                    serviceLocator.getSession(),
                    serviceLocator.getUserEndpoint().getCurrentUserEndpoint().getId(),
                    taskIdToUpdate);
            System.out.println(AdditionalMessage.INSERT_NEW_TASK);
            @NotNull final String newData = Bootstrap.scanner.nextLine();
            if (newData.isEmpty()) break;
            serviceLocator.getTaskEndpoint().mergeTask(
                    serviceLocator.getSession(),
                    serviceLocator.getUserEndpoint().getCurrentUserEndpoint().getId(),
                    newData,
                    taskToUpdate,
                    DataType.NAME);
            System.out.println(AdditionalMessage.TASK_UPDATED);
            taskUpdated = true;
        }
    }

    @Nullable
    private Project findProject() {
        if (serviceLocator.getTaskEndpoint().findAllTask(
                serviceLocator.getSession(),
                serviceLocator.getUserEndpoint().getCurrentUserEndpoint().getId()).isEmpty()) {
            System.out.println(AdditionalMessage.NO_TASKS);
            return null;
        }
        if (serviceLocator.getProjectEndpoint().findAllProject(
                serviceLocator.getSession(),
                serviceLocator.getUserEndpoint().getCurrentUserEndpoint().getId()).isEmpty()) {
            System.out.println(AdditionalMessage.NO_PROJECTS);
            return null;
        }
        System.out.println(AdditionalMessage.CHOOSE_PROJECT);
        @Nullable Project project = null;
        while (project == null) {
            @NotNull final String projectName = Bootstrap.scanner.nextLine();
            if (projectName.isEmpty()) break;
            project = serviceLocator.getProjectEndpoint().findOneByIdProject(
                    serviceLocator.getSession(),
                    serviceLocator.getUserEndpoint().getCurrentUserEndpoint().getId(),
                    projectName);
            if (project == null)
                System.out.println(AdditionalMessage.PROJECT_NAME_DOESNT_EXIST + " " + AdditionalMessage.TRY_AGAIN);
        }
        return project;
    }

    @Override
    public boolean isSecure() {
        return (!(serviceLocator.getUserEndpoint().getCurrentUserEndpoint() == null));
    }
}
