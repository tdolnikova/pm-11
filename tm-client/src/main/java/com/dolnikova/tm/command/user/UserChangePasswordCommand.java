package com.dolnikova.tm.command.user;

import com.dolnikova.tm.bootstrap.Bootstrap;
import com.dolnikova.tm.command.AbstractCommand;
import com.dolnikova.tm.constant.AdditionalMessage;
import com.dolnikova.tm.constant.Command;
import com.dolnikova.tm.endpoint.DataType;
import com.dolnikova.tm.util.HashUtil;
import org.jetbrains.annotations.NotNull;

public final class UserChangePasswordCommand extends AbstractCommand {

    @NotNull
    @Override
    public String command() {
        return Command.USER_CHANGE_PASSWORD;
    }

    @NotNull
    @Override
    public String description() {
        return Command.USER_CHANGE_PASSWORD_DESCRIPTION;
    }

    @Override
    public void execute() throws Exception {
        if (!isSecure()) return;
        System.out.println(AdditionalMessage.ENTER_OLD_PASSWORD);
        @NotNull String oldPassword = "";
        while (oldPassword.isEmpty()) {
            oldPassword = Bootstrap.scanner.nextLine();
        }
        if (!serviceLocator.getUserEndpoint().getCurrentUserEndpoint().getDescription().equals(HashUtil.stringToHashString(oldPassword))) {
            System.out.println(AdditionalMessage.WRONG_PASSWORD);
            return;
        }
        System.out.println(AdditionalMessage.ENTER_NEW_PASSWORD);
        @NotNull String newPassword = "";
        while (newPassword.isEmpty()) {
            newPassword = Bootstrap.scanner.nextLine();
        }
        serviceLocator.getUserEndpoint().mergeUser(
                serviceLocator.getSession(),
                serviceLocator.getUserEndpoint().getCurrentUserEndpoint().getId(),
                newPassword,
                serviceLocator.getUserEndpoint().getCurrentUserEndpoint(),
                DataType.DESCRIPTION);
        System.out.println(AdditionalMessage.PASSWORD_CHANGED_SUCCESSFULLY);
    }

    @Override
    public boolean isSecure() {
        return (!(serviceLocator.getUserEndpoint().getCurrentUserEndpoint() == null));
    }
}
