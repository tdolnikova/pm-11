package com.dolnikova.tm.endpoint;

import com.dolnikova.tm.api.ServiceLocator;
import com.dolnikova.tm.entity.Session;
import com.dolnikova.tm.entity.Task;
import com.dolnikova.tm.entity.User;
import com.dolnikova.tm.enumerated.DataType;
import com.dolnikova.tm.enumerated.Role;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.List;

@NoArgsConstructor
@WebService
public class TaskEndpoint extends AbstractEndpoint {

    public TaskEndpoint(@NotNull ServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    @WebMethod
    @Nullable
    public Task findOneByIdTask(@WebParam(name = "session") @Nullable final Session session,
                                @WebParam (name = "ownerId")  @Nullable String ownerId,
                                @WebParam(name = "id") @Nullable String id) {
        validate(session);
        return taskService.findOneById(ownerId, id);
    }

    @WebMethod
    @Nullable
    public Task findOneByNameTask(@WebParam(name = "session") @Nullable final Session session,
                                  @WebParam(name = "name") @Nullable String name) {
        validate(session);
        return taskService.findOneByName(name);
    }

    @WebMethod
    public @Nullable List<Task> findAllTask(@WebParam(name = "session") @Nullable final Session session,
                                            @WebParam (name = "ownerId") @Nullable String ownerId) {
        validate(session);
        return taskService.findAll(ownerId);
    }

    @WebMethod
    public @Nullable List<Task> findAllByNameTask(@WebParam(name = "session") @Nullable final Session session,
                                                  @WebParam (name = "ownerId") @Nullable String ownerId,
                                                  @WebParam(name = "text") @Nullable String text) {
        validate(session);
        return taskService.findAllByName(ownerId, text);
    }

    @WebMethod
    public @Nullable List<Task> findAllByDescriptionTask(@WebParam(name = "session") @Nullable final Session session,
                                                         @WebParam (name = "ownerId") @Nullable String ownerId,
                                                         @WebParam(name = "text") @Nullable String text) {
        validate(session);
        return taskService.findAllByDescription(ownerId, text);
    }

    @WebMethod
    public void persistTask(@WebParam(name = "session") @Nullable final Session session,
                            @WebParam (name = "ownerId") @Nullable String ownerId,
                            @WebParam(name = "entity") @Nullable Task entity) {
        validate(session);
        taskService.persist(ownerId, entity);
    }

    @WebMethod
    public void persistListTask(@WebParam(name = "session") @Nullable final Session session,
                                @WebParam (name = "ownerId") @Nullable String ownerId,
                                @WebParam(name = "list") @Nullable List<Task> list) {
        validate(session);
        taskService.persistList(ownerId, list);
    }

    @WebMethod
    public void mergeTask(@WebParam(name = "session") @Nullable final Session session,
                          @WebParam (name = "ownerId") @Nullable String ownerId,
                          @WebParam(name = "newData") @Nullable String newData,
                          @WebParam(name = "entityToMerge") @Nullable Task entityToMerge,
                          @WebParam(name = "dataType") @Nullable DataType dataType) {
        validate(session);
        taskService.merge(ownerId, newData, entityToMerge, dataType);
    }

    @WebMethod
    public void removeTask(@WebParam(name = "session") @Nullable final Session session,
                           @WebParam (name = "ownerId") @Nullable String ownerId,
                           @WebParam(name = "entity") @Nullable Task entity) {
        validate(session);
        taskService.remove(ownerId, entity);
    }

    @WebMethod
    public void removeAllTask(@WebParam(name = "session") @Nullable final Session session,
                              @WebParam (name = "ownerId") @Nullable String ownerId) {
        validate(session);
        taskService.removeAll(ownerId);
    }

    @WebMethod
    public boolean saveBinTask(
            @WebParam(name = "session") @Nullable Session session,
            @WebParam(name = "user") @Nullable User user,
            @WebParam(name = "tasks") @Nullable List<Task> entities) throws Exception {
        if (user.getRole() != Role.ADMIN) return false;
        validate(session);
        taskService.saveBin(entities);
        return true;
    }

    @WebMethod
    public boolean saveFasterxmlJsonTask(
            @WebParam(name = "session") @Nullable Session session,
            @WebParam(name = "user") @Nullable User user,
            @WebParam(name = "tasks") @Nullable List<Task> entities) throws Exception {
        if (user.getRole() != Role.ADMIN) return false;
        validate(session);
        taskService.saveFasterxmlJson(entities);
        return true;
    }

    @WebMethod
    public boolean saveFasterxmlXmlTask(
            @WebParam(name = "session") @Nullable Session session,
            @WebParam(name = "user") @Nullable User user,
            @WebParam(name = "tasks") @Nullable List<Task> entities) throws Exception {
        if (user.getRole() != Role.ADMIN) return false;
        validate(session);
        taskService.saveFasterxmlXml(entities);
        return true;
    }

    @WebMethod
    public boolean saveJaxbJsonTask(
            @WebParam(name = "session") @Nullable Session session,
            @WebParam(name = "user") @Nullable User user,
            @WebParam(name = "tasks") @Nullable List<Task> entities) throws Exception {
        if (user.getRole() != Role.ADMIN) return false;
        validate(session);
        taskService.saveJaxbJson(entities);
        return true;
    }

    @WebMethod
    public boolean saveJaxbXmlTask(
            @WebParam(name = "session") @Nullable Session session,
            @WebParam(name = "user") @Nullable User user,
            @WebParam(name = "tasks") @Nullable List<Task> entities) throws Exception {
        if (user.getRole() != Role.ADMIN) return false;
        validate(session);
        taskService.saveJaxbXml(entities);
        return true;
    }


    @WebMethod
    public @Nullable List<Task> loadBinTask(
            @WebParam(name = "session") @Nullable Session session,
            @WebParam(name = "user") @Nullable User user) throws Exception {
        validate(session);
        if (user.getRole() != Role.ADMIN) throw new IllegalAccessException();
        return taskService.loadBin();
    }

    @WebMethod
    public @Nullable List<Task> loadFasterxmlJsonTask(
            @WebParam(name = "session") @Nullable Session session,
            @WebParam(name = "user") @Nullable User user) throws Exception {
        validate(session);
        if (user.getRole() != Role.ADMIN) throw new IllegalAccessException();
        return taskService.loadFasterxmlJson();
    }

    @WebMethod
    public @Nullable List<Task> loadFasterxmlXmlTask(
            @WebParam(name = "session") @Nullable Session session,
            @WebParam(name = "user") @Nullable User user) throws Exception {
        validate(session);
        if (user.getRole() != Role.ADMIN) throw new IllegalAccessException();
        return taskService.loadFasterxmlXml();
    }

    @WebMethod
    public @Nullable List<Task> loadJaxbJsonTask(
            @WebParam(name = "session") @Nullable Session session,
            @WebParam(name = "user") @Nullable User user) throws Exception {
        validate(session);
        if (user.getRole() != Role.ADMIN) throw new IllegalAccessException();
        return taskService.loadJaxbJson();
    }

    @WebMethod
    public @Nullable List<Task> loadJaxbXmlTask(
            @WebParam(name = "session") @Nullable Session session,
            @WebParam(name = "user") @Nullable User user) throws Exception {
        validate(session);
        if (user.getRole() != Role.ADMIN) throw new IllegalAccessException();
        return taskService.loadJaxbXml();
    }

}
