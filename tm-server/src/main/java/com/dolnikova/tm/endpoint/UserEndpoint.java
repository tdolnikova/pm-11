package com.dolnikova.tm.endpoint;

import com.dolnikova.tm.api.ServiceLocator;
import com.dolnikova.tm.entity.Session;
import com.dolnikova.tm.entity.User;
import com.dolnikova.tm.enumerated.DataType;
import com.dolnikova.tm.enumerated.Role;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.List;

@NoArgsConstructor
@WebService
public class UserEndpoint extends AbstractEndpoint {

    public UserEndpoint(@NotNull ServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    @WebMethod
    @Nullable
    public User findOneByIdUser(@WebParam(name = "session") @Nullable Session session,
                                @WebParam(name = "ownerId") @Nullable String ownerId,
                                @WebParam(name = "id") @Nullable String id) {
        validate(session);
        return userService.findOneById(ownerId, id);
    }

    @WebMethod
    @Nullable
    public User findOneByNameUser(@WebParam(name = "name") @Nullable String name) {
        return userService.findOneByName(name);
    }

    @WebMethod
    public @Nullable User findOneBySession(@WebParam(name = "session") @Nullable Session session) {
        validate(session);
        return userService.findOneBySession(session);
    }

    @WebMethod
    public @Nullable List<User> findAllUser(@WebParam(name = "session") @Nullable Session session,
                                            @WebParam(name = "ownerId") @Nullable String ownerId) {
        validate(session);
        return userService.findAll(ownerId);
    }

    @WebMethod
    public @Nullable List<User> findAllByNameUser(@WebParam(name = "session") @Nullable Session session,
                                                  @WebParam(name = "ownerId") @Nullable String ownerId,
                                                  @WebParam(name = "text") @Nullable String text) {
        validate(session);
        return userService.findAllByName(ownerId, text);
    }

    @WebMethod
    public @Nullable List<User> findAllByDescriptionUser(@WebParam(name = "session") @Nullable Session session,
                                                         @WebParam(name = "ownerId") @Nullable String ownerId,
                                                         @WebParam(name = "text") @Nullable String text) {
        validate(session);
        return userService.findAllByDescription(ownerId, text);
    }

    @WebMethod
    public void persistUser(@WebParam(name = "entity") @Nullable User entity) {
        System.out.println("Принято: " + entity.getName() + " " + entity.getId() + " " + entity.getOwnerId());
        userService.persist(entity.getId(), entity);
    }

    @WebMethod
    public void persistListUser(@WebParam(name = "session") @Nullable Session session,
                                @WebParam(name = "ownerId") @Nullable String ownerId,
                                @WebParam(name = "list") @Nullable List<User> list) {
        validate(session);
        userService.persistList(ownerId, list);
    }

    @WebMethod
    public void mergeUser(@WebParam(name = "session") @Nullable Session session,
                          @WebParam(name = "ownerId") @Nullable String ownerId,
                          @WebParam(name = "newData") @Nullable String newData,
                          @WebParam(name = "entityToMerge") @Nullable User entityToMerge,
                          @WebParam(name = "dataType") @Nullable DataType dataType) {
        validate(session);
        userService.merge(ownerId, newData, entityToMerge, dataType);
    }

    @WebMethod
    public void removeUser(@WebParam(name = "session") @Nullable Session session,
                           @WebParam(name = "ownerId") @Nullable String ownerId,
                           @WebParam(name = "entity") @Nullable User entity) {
        validate(session);
        userService.remove(ownerId, entity);
    }

    @WebMethod
    public void removeAllUser(@WebParam(name = "session") @Nullable Session session,
                              @WebParam(name = "ownerId") @Nullable String ownerId) {
        userService.removeAll(ownerId);
    }

    @WebMethod
    @Nullable
    public User getCurrentUserEndpoint() {
        return userService.getCurrentUser();
    }

    @WebMethod
    public void setCurrentUserEndpoint(@WebParam(name = "session") @Nullable Session session,
                                       @WebParam(name = "currentUser") @Nullable final User currentUser) {
        validate(session);
        userService.setCurrentUser(currentUser);
    }

    @WebMethod
    public boolean saveBinUser(
            @WebParam(name = "session") @Nullable Session session,
            @WebParam(name = "user") @Nullable User user,
            @WebParam(name = "users") @Nullable List<User> entities) throws Exception {
        if (user.getRole() != Role.ADMIN) return false;
        validate(session);
        userService.saveBin(entities);
        return true;
    }

    @WebMethod
    public boolean saveFasterxmlJsonUser(
            @WebParam(name = "session") @Nullable Session session,
            @WebParam(name = "user") @Nullable User user,
            @WebParam(name = "users") @Nullable List<User> entities) throws Exception {
        if (user.getRole() != Role.ADMIN) return false;
        validate(session);
        userService.saveFasterxmlJson(entities);
        return true;
    }

    @WebMethod
    public boolean saveFasterxmlXmlUser(
            @WebParam(name = "session") @Nullable Session session,
            @WebParam(name = "user") @Nullable User user,
            @WebParam(name = "users") @Nullable List<User> entities) throws Exception {
        if (user.getRole() != Role.ADMIN) return false;
        validate(session);
        userService.saveFasterxmlXml(entities);
        return true;
    }

    @WebMethod
    public boolean saveJaxbJsonUser(
            @WebParam(name = "session") @Nullable Session session,
            @WebParam(name = "user") @Nullable User user,
            @WebParam(name = "users") @Nullable List<User> entities) throws Exception {
        if (user.getRole() != Role.ADMIN) return false;
        validate(session);
        userService.saveJaxbJson(entities);
        return true;
    }

    @WebMethod
    public boolean saveJaxbXmlUser(
            @WebParam(name = "session") @Nullable Session session,
            @WebParam(name = "user") @Nullable User user,
            @WebParam(name = "users") @Nullable List<User> entities) throws Exception {
        if (user.getRole() != Role.ADMIN) return false;
        validate(session);
        userService.saveJaxbXml(entities);
        return true;
    }

    @WebMethod
    public @Nullable List<User> loadBinUser(
            @WebParam(name = "session") @Nullable Session session,
            @WebParam(name = "user") @Nullable User user) throws Exception {
        validate(session);
        if (user.getRole() != Role.ADMIN) throw new IllegalAccessException();
        return userService.loadBin();
    }

    @WebMethod
    public @Nullable List<User> loadFasterxmlJsonUser(
            @WebParam(name = "session") @Nullable Session session,
            @WebParam(name = "user") @Nullable User user) throws Exception {
        validate(session);
        if (user.getRole() != Role.ADMIN) throw new IllegalAccessException();
        return userService.loadFasterxmlJson();
    }

    @WebMethod
    public @Nullable List<User> loadFasterxmlXmlUser(
            @WebParam(name = "session") @Nullable Session session,
            @WebParam(name = "user") @Nullable User user) throws Exception {
        validate(session);
        if (user.getRole() != Role.ADMIN) throw new IllegalAccessException();
        return userService.loadFasterxmlXml();
    }

    @WebMethod
    public @Nullable List<User> loadJaxbJsonUser(
            @WebParam(name = "session") @Nullable Session session,
            @WebParam(name = "user") @Nullable User user) throws Exception {
        validate(session);
        if (user.getRole() != Role.ADMIN) throw new IllegalAccessException();
        return userService.loadJaxbJson();
    }

    @WebMethod
    public @Nullable List<User> loadJaxbXmlUser(
            @WebParam(name = "session") @Nullable Session session,
            @WebParam(name = "user") @Nullable User user) throws Exception {
        validate(session);
        if (user.getRole() != Role.ADMIN) throw new IllegalAccessException();
        return userService.loadJaxbXml();
    }

}
