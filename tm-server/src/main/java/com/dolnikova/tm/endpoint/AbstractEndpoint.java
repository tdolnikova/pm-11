package com.dolnikova.tm.endpoint;

import com.dolnikova.tm.api.ServiceLocator;
import com.dolnikova.tm.api.service.*;
import com.dolnikova.tm.entity.Session;
import com.dolnikova.tm.exception.SessionExpiredException;
import com.dolnikova.tm.exception.SessionNotFoundException;
import com.dolnikova.tm.util.SignatureUtil;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

@NoArgsConstructor
abstract class AbstractEndpoint {

    @NotNull ServiceLocator serviceLocator;
    @NotNull IProjectService projectService;
    @NotNull ITaskService taskService;
    @NotNull IUserService userService;
    @NotNull ISessionService sessionService;

    AbstractEndpoint(@NotNull final ServiceLocator serviceLocator) {
        this.serviceLocator = serviceLocator;
        this.projectService = serviceLocator.getProjectService();
        this.taskService = serviceLocator.getTaskService();
        this.userService = serviceLocator.getUserService();
        this.sessionService = serviceLocator.getSessionService();
    }

    void validate(@Nullable final Session session) {
        try {
            if (session == null) throw new SessionNotFoundException();

            @NotNull final Long currentTime = System.currentTimeMillis();
            @NotNull final Long sessionTime = session.getDate();
            if ((currentTime - sessionTime > 100000)) throw new SessionExpiredException();

            @Nullable final String sessionSignature = session.getSignature();
            @Nullable final String calculatedSignature = SignatureUtil.sign(session, "JAVA", 3);
            if (sessionSignature == null
                    || sessionSignature.isEmpty()
                    || !sessionSignature.equals(calculatedSignature)) {
                throw new SessionNotFoundException();
            }
            Session foundSession = sessionService.findOneById(session.getUserId(), session.getId());
            if (foundSession == null) throw new SessionNotFoundException();
        } catch (SessionNotFoundException | SessionExpiredException e) {
            e.getMessage();
        }
    }

}
