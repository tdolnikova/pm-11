package com.dolnikova.tm.endpoint;

import com.dolnikova.tm.api.ServiceLocator;
import com.dolnikova.tm.entity.Project;
import com.dolnikova.tm.entity.Session;
import com.dolnikova.tm.entity.User;
import com.dolnikova.tm.enumerated.DataType;
import com.dolnikova.tm.enumerated.Role;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.List;

@NoArgsConstructor
@WebService
public class ProjectEndpoint extends AbstractEndpoint {

    public ProjectEndpoint(@NotNull ServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    @WebMethod
    @Nullable
    public Project findOneByIdProject(@WebParam(name = "session") @Nullable final Session session,
                                      @WebParam(name = "ownerId") @Nullable String ownerId,
                                      @WebParam(name = "id") @Nullable String id) {
        validate(session);
        return projectService.findOneById(ownerId, id);
    }

    @WebMethod
    @Nullable
    public Project findOneByNameProject(@WebParam(name = "session") @Nullable final Session session,
                                        @WebParam(name = "name") @Nullable String name) {
        validate(session);
        return projectService.findOneByName(name);
    }

    @WebMethod
    public @Nullable List<Project> findAllProject(@WebParam(name = "session") @Nullable final Session session,
                                                  @WebParam(name = "ownerId") @Nullable String ownerId) {
        validate(session);
        return projectService.findAll(ownerId);
    }

    @WebMethod
    public @Nullable List<Project> findAllByNameProject(@WebParam(name = "session") @Nullable final Session session,
                                                        @WebParam(name = "ownerId") @Nullable String ownerId,
                                                        @WebParam(name = "text") @Nullable String text) {
        validate(session);
        return projectService.findAllByName(ownerId, text);
    }

    @WebMethod
    public @Nullable List<Project> findAllByDescriptionProject(@WebParam(name = "session") @Nullable final Session session,
                                                               @WebParam(name = "ownerId") @Nullable String ownerId,
                                                               @WebParam(name = "text") @Nullable String text) {
        validate(session);
        return projectService.findAllByDescription(ownerId, text);
    }

    @WebMethod
    public void persistProject(@WebParam(name = "session") @Nullable final Session session,
                               @WebParam(name = "ownerId") @Nullable String ownerId,
                               @WebParam(name = "entity") @Nullable Project entity) {
        validate(session);
        projectService.persist(ownerId, entity);
    }

    @WebMethod
    public void persistListProject(@WebParam(name = "session") @Nullable final Session session,
                                   @WebParam(name = "ownerId") @Nullable String ownerId,
                                   @WebParam(name = "list") @Nullable List<Project> list) {
        validate(session);
        projectService.persistList(ownerId, list);
    }

    @WebMethod
    public void mergeProject(@WebParam(name = "session") @Nullable final Session session,
                             @WebParam(name = "ownerId") @Nullable String ownerId,
                             @WebParam(name = "newData") @Nullable String newData,
                             @WebParam(name = "entityToMerge") @Nullable Project entityToMerge,
                             @WebParam(name = "dataType") @Nullable DataType dataType) {
        validate(session);
        projectService.merge(ownerId, newData, entityToMerge, dataType);
    }

    @WebMethod
    public void removeProject(@WebParam(name = "session") @Nullable final Session session,
                              @WebParam(name = "ownerId") @Nullable String ownerId,
                              @WebParam(name = "entity") @Nullable Project entity) {
        validate(session);
        projectService.remove(ownerId, entity);
    }

    @WebMethod
    public void removeAllProject(@WebParam(name = "session") @Nullable final Session session,
                                 @WebParam(name = "ownerId") @Nullable String ownerId) {
        validate(session);
        projectService.removeAll(ownerId);
    }

    @WebMethod
    public boolean saveBinProject(
            @WebParam(name = "session") @Nullable Session session,
            @WebParam(name = "user") @Nullable User user,
            @WebParam(name = "projects") @Nullable List<Project> entities) throws Exception {
        if (user.getRole() != Role.ADMIN) return false;
        validate(session);
        projectService.saveBin(entities);
        return true;
    }

    @WebMethod
    public boolean saveFasterxmlJsonProject(
            @WebParam(name = "session") @Nullable Session session,
            @WebParam(name = "user") @Nullable User user,
            @WebParam(name = "projects") @Nullable List<Project> entities) throws Exception {
        if (user.getRole() != Role.ADMIN) return false;
        validate(session);
        projectService.saveFasterxmlJson(entities);
        return true;
    }

    @WebMethod
    public boolean saveFasterxmlXmlProject(
            @WebParam(name = "session") @Nullable Session session,
            @WebParam(name = "user") @Nullable User user,
            @WebParam(name = "projects") @Nullable List<Project> entities) throws Exception {
        if (user.getRole() != Role.ADMIN) return false;
        validate(session);
        projectService.saveFasterxmlXml(entities);
        System.out.println(this.getClass().getName());
        return true;
    }

    @WebMethod
    public boolean saveJaxbJsonProject(
            @WebParam(name = "session") @Nullable Session session,
            @WebParam(name = "user") @Nullable User user,
            @WebParam(name = "projects") @Nullable List<Project> entities) throws Exception {
        if (user.getRole() != Role.ADMIN) return false;
        validate(session);
        projectService.saveJaxbJson(entities);
        return true;
    }

    @WebMethod
    public boolean saveJaxbXmlProject(
            @WebParam(name = "session") @Nullable Session session,
            @WebParam(name = "user") @Nullable User user,
            @WebParam(name = "projects") @Nullable List<Project> entities) throws Exception {
        if (user.getRole() != Role.ADMIN) return false;
        validate(session);
        projectService.saveJaxbXml(entities);
        return true;
    }

    @WebMethod
    public @Nullable List<Project> loadBinProject(
            @WebParam(name = "session") @Nullable Session session,
            @WebParam(name = "user") @Nullable User user) throws Exception {
        validate(session);
        if (user.getRole() != Role.ADMIN) throw new IllegalAccessException();
        return projectService.loadBin();
    }

    @WebMethod
    public @Nullable List<Project> loadFasterxmlJsonProject(
            @WebParam(name = "session") @Nullable Session session,
            @WebParam(name = "user") @Nullable User user) throws Exception {
        validate(session);
        if (user.getRole() != Role.ADMIN) throw new IllegalAccessException();
        return projectService.loadFasterxmlJson();
    }

    @WebMethod
    public @Nullable List<Project> loadFasterxmlXmlProject(
            @WebParam(name = "session") @Nullable Session session,
            @WebParam(name = "user") @Nullable User user) throws Exception {
        validate(session);
        if (user.getRole() != Role.ADMIN) throw new IllegalAccessException();
        return projectService.loadFasterxmlXml();
    }

    @WebMethod
    public @Nullable List<Project> loadJaxbJsonProject(
            @WebParam(name = "session") @Nullable Session session,
            @WebParam(name = "user") @Nullable User user) throws Exception {
        validate(session);
        if (user.getRole() != Role.ADMIN) throw new IllegalAccessException();
        return projectService.loadJaxbJson();
    }

    @WebMethod
    public @Nullable List<Project> loadJaxbXmlProject(
            @WebParam(name = "session") @Nullable Session session,
            @WebParam(name = "user") @Nullable User user) throws Exception {
        validate(session);
        if (user.getRole() != Role.ADMIN) throw new IllegalAccessException();
        return projectService.loadJaxbXml();
    }

}
