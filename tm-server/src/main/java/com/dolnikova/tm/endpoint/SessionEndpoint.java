package com.dolnikova.tm.endpoint;

import com.dolnikova.tm.api.ServiceLocator;
import com.dolnikova.tm.entity.Session;
import com.dolnikova.tm.entity.User;
import com.dolnikova.tm.enumerated.Role;
import com.dolnikova.tm.util.PasswordHashUtil;
import com.dolnikova.tm.util.SignatureUtil;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.List;

@NoArgsConstructor
@WebService
public class SessionEndpoint extends AbstractEndpoint {

    public SessionEndpoint(@NotNull ServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    @WebMethod
    public Session getSession(@WebParam(name = "login") @Nullable final String login,
                              @WebParam(name = "password") @Nullable final String password) {
        if (login == null || login.isEmpty()) return null;
        if (password == null || password.isEmpty()) return null;
        User user = userService.findOneByName(login);
        String hashedPassword = PasswordHashUtil.md5(password);
        if (user == null || !hashedPassword.equals(user.getDescription())) {
            System.out.println("User not found");
            return null;
        }
        @NotNull final Session session = new Session();
        session.setUserId(userService.findOneByName(login).getId());
        @Nullable final String signature = SignatureUtil.sign(password, "JAVA", 3);
        session.setSignature(signature);
        return session;
    }

    @WebMethod
    public boolean saveBinSession(
            @WebParam(name = "session") @Nullable Session session,
            @WebParam(name = "user") @Nullable User user,
            @WebParam(name = "sessions") @Nullable List<Session> entities) throws Exception {
        if (user.getRole() != Role.ADMIN) return false;
        validate(session);
        sessionService.saveBin(entities);
        return true;
    }

    @WebMethod
    public boolean saveFasterxmlJsonSession(
            @WebParam(name = "session") @Nullable Session session,
            @WebParam(name = "user") @Nullable User user,
            @WebParam(name = "sessions") @Nullable List<Session> entities) throws Exception {
        if (user.getRole() != Role.ADMIN) return false;
        validate(session);
        sessionService.saveFasterxmlJson(entities);
        return true;
    }

    @WebMethod
    public boolean saveFasterxmlXmlSession(
            @WebParam(name = "session") @Nullable Session session,
            @WebParam(name = "user") @Nullable User user,
            @WebParam(name = "sessions") @Nullable List<Session> entities) throws Exception {
        if (user.getRole() != Role.ADMIN) return false;
        validate(session);
        sessionService.saveFasterxmlXml(entities);
        return true;
    }

    @WebMethod
    public boolean saveJaxbJsonSession(
            @WebParam(name = "session") @Nullable Session session,
            @WebParam(name = "user") @Nullable User user,
            @WebParam(name = "sessions") @Nullable List<Session> entities) throws Exception {
        if (user.getRole() != Role.ADMIN) return false;
        validate(session);
        sessionService.saveJaxbJson(entities);
        return true;
    }

    @WebMethod
    public boolean saveJaxbXmlSession(
            @WebParam(name = "session") @Nullable Session session,
            @WebParam(name = "user") @Nullable User user,
            @WebParam(name = "sessions") @Nullable List<Session> entities) throws Exception {
        if (user.getRole() != Role.ADMIN) return false;
        validate(session);
        sessionService.saveJaxbXml(entities);
        return true;
    }

    @WebMethod
    public @Nullable List<Session> loadBinSession(
            @WebParam(name = "session") @Nullable Session session,
            @WebParam(name = "user") @Nullable User user) throws Exception {
        validate(session);
        if (user.getRole() != Role.ADMIN) throw new IllegalAccessException();
        return sessionService.loadBin();
    }

    @WebMethod
    public @Nullable List<Session> loadFasterxmlJsonSession(
            @WebParam(name = "session") @Nullable Session session,
            @WebParam(name = "user") @Nullable User user) throws Exception {
        validate(session);
        if (user.getRole() != Role.ADMIN) throw new IllegalAccessException();
        return sessionService.loadFasterxmlJson();
    }

    @WebMethod
    public @Nullable List<Session> loadFasterxmlXmlSession(
            @WebParam(name = "session") @Nullable Session session,
            @WebParam(name = "user") @Nullable User user) throws Exception {
        validate(session);
        if (user.getRole() != Role.ADMIN) throw new IllegalAccessException();
        return sessionService.loadFasterxmlXml();
    }

    @WebMethod
    public @Nullable List<Session> loadJaxbJsonSession(
            @WebParam(name = "session") @Nullable Session session,
            @WebParam(name = "user") @Nullable User user) throws Exception {
        validate(session);
        if (user.getRole() != Role.ADMIN) throw new IllegalAccessException();
        return sessionService.loadJaxbJson();
    }

    @WebMethod
    public @Nullable List<Session> loadJaxbXmlSession(
            @WebParam(name = "session") @Nullable Session session,
            @WebParam(name = "user") @Nullable User user) throws Exception {
        validate(session);
        if (user.getRole() != Role.ADMIN) throw new IllegalAccessException();
        return sessionService.loadJaxbXml();
    }

}
