package com.dolnikova.tm.repository;

import com.dolnikova.tm.api.repository.ISessionRepository;
import com.dolnikova.tm.constant.General;
import com.dolnikova.tm.entity.Session;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import java.io.*;
import java.util.ArrayList;
import java.util.List;

@NoArgsConstructor
public final class SessionRepository extends AbstractRepository<Session> implements ISessionRepository {

    @Override
    public void saveBin(@NotNull final List<Session> entities) throws Exception {
        @Nullable final File path = new File(General.SAVE_PATH + General.SESSIONS_BIN);
        if (!path.exists()) {
            path.getParentFile().mkdirs();
            path.createNewFile();
        }
        try (@Nullable final ObjectOutputStream objectOutputStream = new ObjectOutputStream(new FileOutputStream(path))) {
            objectOutputStream.writeObject(new ArrayList<>(entities));
        }
    }

    @Override
    public void saveFasterxmlJson(@NotNull final List<Session> entities) throws Exception {
        @Nullable final File path = new File(General.SAVE_PATH + General.SESSIONS_JSON);
        if (!path.exists()) {
            path.getParentFile().mkdirs();
            path.createNewFile();
        }
        @Nullable final ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.writerWithDefaultPrettyPrinter().writeValue(path, new ArrayList<>(entities));
    }

    @Override
    public void saveFasterxmlXml(@NotNull final List<Session> entities) throws Exception {
        @Nullable final File path = new File(General.SAVE_PATH + General.SESSIONS_XML);
        if (!path.exists()) {
            path.getParentFile().mkdirs();
            path.createNewFile();
        }
        @Nullable final XmlMapper xmlMapper = new XmlMapper();
        xmlMapper.writerWithDefaultPrettyPrinter().writeValue(path, new ArrayList<>(entities));
    }

    @Override
    public void saveJaxbJson(@NotNull final List<Session> entities) throws Exception {
        @Nullable final File path = new File(General.SAVE_PATH + General.SESSIONS_JSON);
        if (!path.exists()) {
            path.getParentFile().mkdirs();
            path.createNewFile();
        }
        @Nullable final JAXBContext jaxbContext = JAXBContext.newInstance(ArrayList.class);
        @Nullable final Marshaller marshaller = jaxbContext.createMarshaller();
        marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
        marshaller.marshal(new ArrayList<>(entities), path);
    }

    @Override
    public void saveJaxbXml(@NotNull final List<Session> entities) throws Exception {
        @Nullable final File path = new File(General.SAVE_PATH + General.SESSIONS_XML);
        if (!path.exists()) {
            path.getParentFile().mkdirs();
            path.createNewFile();
        }
        @Nullable final JAXBContext jaxbContext = JAXBContext.newInstance(ArrayList.class);
        @Nullable final Marshaller marshaller = jaxbContext.createMarshaller();
        marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
        marshaller.marshal(new ArrayList<>(entities), path);
    }

    @Nullable
    @Override
    public List<Session> loadBin() throws Exception {
        @Nullable final File file = new File(General.SAVE_PATH + General.SESSIONS_BIN);
        try (@Nullable final ObjectInputStream objectInputStream = new ObjectInputStream(new FileInputStream(file))) {
            @Nullable final ArrayList<Session> sessions = (ArrayList) objectInputStream.readObject();
            return sessions;
        }
    }

    @Nullable
    @Override
    public List<Session> loadFasterxmlJson() throws Exception {
        @Nullable final File file = new File(General.SAVE_PATH + General.SESSIONS_JSON);
        @Nullable final ObjectMapper objectMapper = new ObjectMapper();
        @Nullable final ArrayList<Session> sessions = objectMapper.readValue(file, ArrayList.class);
        return sessions;
    }

    @Nullable
    @Override
    public List<Session> loadFasterxmlXml() throws Exception {
        @Nullable final File file = new File(General.SAVE_PATH + General.SESSIONS_XML);
        @Nullable final XmlMapper xmlMapper = new XmlMapper();
        @Nullable final ArrayList<Session> sessions = xmlMapper.readValue(file, ArrayList.class);
        return sessions;
    }

    @Nullable
    @Override
    public List<Session> loadJaxbJson() throws Exception {
        @Nullable final File file = new File(General.SAVE_PATH + General.SESSIONS_JSON);
        @Nullable final JAXBContext jaxbContext = JAXBContext.newInstance(ArrayList.class);
        @Nullable final Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();
        @Nullable final ArrayList<Session> sessions = (ArrayList<Session>) unmarshaller.unmarshal(file);
        return sessions;
    }

    @Nullable
    @Override
    public List<Session> loadJaxbXml() throws Exception {
        @Nullable final File file = new File(General.SAVE_PATH + General.SESSIONS_XML);
        @Nullable final JAXBContext jaxbContext = JAXBContext.newInstance(ArrayList.class);
        @Nullable final Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();
        @Nullable final ArrayList<Session> sessions = (ArrayList<Session>) unmarshaller.unmarshal(file);
        return sessions;
    }

}
