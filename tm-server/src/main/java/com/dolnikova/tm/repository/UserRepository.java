package com.dolnikova.tm.repository;

import com.dolnikova.tm.api.repository.IUserRepository;
import com.dolnikova.tm.constant.General;
import com.dolnikova.tm.entity.Session;
import com.dolnikova.tm.entity.User;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@NoArgsConstructor
public final class UserRepository extends AbstractRepository<User> implements IUserRepository {

    @Override
    public @Nullable User findOneBySession(@NotNull Session session) {
        for (Map.Entry<String, User> entity : entities.entrySet()) {
            @NotNull final User foundEntity = entity.getValue();
            if (foundEntity.getId().equals(session.getUserId())) {
                return foundEntity;
            }
        }
        return null;
    }

    @Override
    public void saveBin(@NotNull final List<User> entities) throws Exception {
        @Nullable final File path = new File(General.SAVE_PATH + General.USERS_BIN);
        if (!path.exists()) {
            path.getParentFile().mkdirs();
            path.createNewFile();
        }
        try (@Nullable final ObjectOutputStream objectOutputStream = new ObjectOutputStream(new FileOutputStream(path))) {
            objectOutputStream.writeObject(new ArrayList<>(entities));
        }
    }

    @Override
    public void saveFasterxmlJson(@NotNull final List<User> entities) throws Exception {
        @Nullable final File path = new File(General.SAVE_PATH + General.USERS_JSON);
        if (!path.exists()) {
            path.getParentFile().mkdirs();
            path.createNewFile();
        }
        @Nullable final ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.writerWithDefaultPrettyPrinter().writeValue(path, new ArrayList<>(entities));
    }

    @Override
    public void saveFasterxmlXml(@NotNull final List<User> entities) throws Exception {
        @Nullable final File path = new File(General.SAVE_PATH + General.USERS_XML);
        if (!path.exists()) {
            path.getParentFile().mkdirs();
            path.createNewFile();
        }
        @Nullable final XmlMapper xmlMapper = new XmlMapper();
        xmlMapper.writerWithDefaultPrettyPrinter().writeValue(path, new ArrayList<>(entities));
    }

    @Override
    public void saveJaxbJson(@NotNull final List<User> entities) throws Exception {
        @Nullable final File path = new File(General.SAVE_PATH + General.USERS_JSON);
        if (!path.exists()) {
            path.getParentFile().mkdirs();
            path.createNewFile();
        }
        @Nullable final JAXBContext jaxbContext = JAXBContext.newInstance(ArrayList.class);
        @Nullable final Marshaller marshaller = jaxbContext.createMarshaller();
        marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
        marshaller.marshal(new ArrayList<>(entities), path);
    }

    @Override
    public void saveJaxbXml(@NotNull final List<User> entities) throws Exception {
        @Nullable final File path = new File(General.SAVE_PATH + General.USERS_XML);
        if (!path.exists()) {
            path.getParentFile().mkdirs();
            path.createNewFile();
        }
        @Nullable final JAXBContext jaxbContext = JAXBContext.newInstance(ArrayList.class);
        @Nullable final Marshaller marshaller = jaxbContext.createMarshaller();
        marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
        marshaller.marshal(new ArrayList<>(entities), path);
    }

    @Nullable
    @Override
    public List<User> loadBin() throws Exception {
        @Nullable final File file = new File(General.SAVE_PATH + General.USERS_BIN);
        try (@Nullable final ObjectInputStream objectInputStream = new ObjectInputStream(new FileInputStream(file))) {
            @Nullable final ArrayList<User> users = (ArrayList) objectInputStream.readObject();
            return users;
        }
    }

    @Nullable
    @Override
    public List<User> loadFasterxmlJson() throws Exception {
        @Nullable final File file = new File(General.SAVE_PATH + General.USERS_JSON);
        @Nullable final ObjectMapper objectMapper = new ObjectMapper();
        @Nullable final ArrayList<User> users = objectMapper.readValue(file, ArrayList.class);
        return users;
    }

    @Nullable
    @Override
    public List<User> loadFasterxmlXml() throws Exception {
        @Nullable final File file = new File(General.SAVE_PATH + General.USERS_XML);
        @Nullable final XmlMapper xmlMapper = new XmlMapper();
        @Nullable final ArrayList<User> users = xmlMapper.readValue(file, ArrayList.class);
        return users;
    }

    @Nullable
    @Override
    public List<User> loadJaxbJson() throws Exception {
        @Nullable final File file = new File(General.SAVE_PATH + General.USERS_JSON);
        @Nullable final JAXBContext jaxbContext = JAXBContext.newInstance(ArrayList.class);
        @Nullable final Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();
        @Nullable final ArrayList<User> users = (ArrayList<User>) unmarshaller.unmarshal(file);
        return users;
    }

    @Nullable
    @Override
    public List<User> loadJaxbXml() throws Exception {
        @Nullable final File file = new File(General.SAVE_PATH + General.USERS_XML);
        @Nullable final JAXBContext jaxbContext = JAXBContext.newInstance(ArrayList.class);
        @Nullable final Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();
        @Nullable final ArrayList<User> users = (ArrayList<User>) unmarshaller.unmarshal(file);
        return users;
    }

}
