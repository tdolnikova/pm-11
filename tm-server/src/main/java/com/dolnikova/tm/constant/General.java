package com.dolnikova.tm.constant;

import java.io.File;

public final class General {

    public final static String ID = "ID";
    public final static String SAVE_PATH =
            System.getProperty("user.dir") + File.separator
                    + "tm-server" + File.separator
                    + "src" + File.separator
                    + "main" + File.separator
                    + "resources" + File.separator;
    public final static String ADDRESS = "http://localhost:8080/";

    public final static String PROJECTS_BIN = "projects.bin";
    public final static String PROJECTS_JSON = "projects.json";
    public final static String PROJECTS_XML = "projects.xml";

    public final static String TASKS_BIN = "tasks.bin";
    public final static String TASKS_JSON = "tasks.json";
    public final static String TASKS_XML = "tasks.xml";

    public final static String SESSIONS_BIN = "sessions.bin";
    public final static String SESSIONS_JSON = "sessions.json";
    public final static String SESSIONS_XML = "sessions.xml";

    public final static String USERS_BIN = "users.bin";
    public final static String USERS_JSON = "users.json";
    public final static String USERS_XML = "users.xml";

}
