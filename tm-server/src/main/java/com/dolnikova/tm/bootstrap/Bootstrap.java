package com.dolnikova.tm.bootstrap;

import com.dolnikova.tm.api.ServiceLocator;
import com.dolnikova.tm.api.repository.IProjectRepository;
import com.dolnikova.tm.api.repository.ISessionRepository;
import com.dolnikova.tm.api.repository.ITaskRepository;
import com.dolnikova.tm.api.repository.IUserRepository;
import com.dolnikova.tm.api.service.*;
import com.dolnikova.tm.constant.General;
import com.dolnikova.tm.endpoint.*;
import com.dolnikova.tm.entity.Project;
import com.dolnikova.tm.entity.Task;
import com.dolnikova.tm.entity.User;
import com.dolnikova.tm.enumerated.Role;
import com.dolnikova.tm.repository.ProjectRepository;
import com.dolnikova.tm.repository.SessionRepository;
import com.dolnikova.tm.repository.TaskRepository;
import com.dolnikova.tm.repository.UserRepository;
import com.dolnikova.tm.service.*;
import com.dolnikova.tm.util.DateUtil;
import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;

import javax.xml.ws.Endpoint;
import java.util.Scanner;

@Getter
@Setter
public final class Bootstrap implements ServiceLocator {

    @NotNull private final IProjectRepository projectRepository = new ProjectRepository();
    @NotNull private final ITaskRepository taskRepository = new TaskRepository();
    @NotNull private final IUserRepository userRepository = new UserRepository();
    @NotNull private final ISessionRepository sessionRepository = new SessionRepository();

    @NotNull private final IUserService userService = new UserService(userRepository);
    @NotNull private final ITaskService taskService = new TaskService(taskRepository);
    @NotNull private final IProjectService projectService = new ProjectService(projectRepository);
    @NotNull private final ISessionService sessionService = new SessionService(sessionRepository);

    public void init() {
        @NotNull final User user = new User();
        user.setName("user");
        user.setDescription("user");
        user.setRole(Role.USER);
        registryUser(user);
        @NotNull final User admin = new User();
        admin.setName("admin");
        admin.setDescription("admin");
        admin.setRole(Role.ADMIN);
        registryUser(admin);
        userService.setCurrentUser(user);
        insertDullData(user);
        start();
    }

    private void start() {
        Endpoint.publish(General.ADDRESS + "TaskEndpoint?WSDL", new TaskEndpoint(this));
        Endpoint.publish(General.ADDRESS + "ProjectEndpoint?WSDL", new ProjectEndpoint(this));
        Endpoint.publish(General.ADDRESS + "UserEndpoint?WSDL", new UserEndpoint(this));
        Endpoint.publish(General.ADDRESS + "SessionEndpoint?WSDL", new SessionEndpoint(this));

        System.out.println(General.ADDRESS + "TaskEndpoint?WSDL");
        System.out.println(General.ADDRESS + "ProjectEndpoint?WSDL");
        System.out.println(General.ADDRESS + "UserEndpoint?WSDL");
        System.out.println(General.ADDRESS + "SessionEndpoint?WSDL");

        Scanner scanner = new Scanner(System.in);
        String input = "";
        while (!"exit".equals(input)) {
            try {
                input = scanner.nextLine();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        System.exit(0);
    }

    private void registryUser(@NotNull final User user) {
        if (user.getName() == null || user.getName().isEmpty()) return;
        if (user.getDescription() == null || user.getDescription().isEmpty()) return;
        if (user.getRole() == null) return;
        userService.persist(user.getId(), user);
    }

    @Override
    public @NotNull IProjectService getProjectService() {
        return projectService;
    }

    @Override
    public @NotNull ITaskService getTaskService() {
        return taskService;
    }

    @Override
    public @NotNull IUserService getUserService() {
        return userService;
    }

    @Override
    public @NotNull ISessionService getSessionService() {
        return sessionService;
    }


    private void insertDullData(User user) {
        Project project = new Project(user.getId(), "qwerty");
        project.setStartDate(DateUtil.stringToDate("01.01.1998"));
        project.setEndDate(DateUtil.stringToDate("12.05.1999"));
        projectService.persist(user.getId(), project);
        Task task1 = new Task(user.getId(), "task1");
        task1.setProjectId(project.getId());
        task1.setStartDate(DateUtil.stringToDate("03.11.2019"));
        task1.setEndDate(DateUtil.stringToDate("03.07.2020"));
        Task task2 = new Task(user.getId(), "task2");
        task2.setProjectId(project.getId());
        task2.setStartDate(DateUtil.stringToDate("10.06.1999"));
        task2.setEndDate(DateUtil.stringToDate("12.08.2000"));
        taskService.persist(user.getId(), task1);
        taskService.persist(user.getId(), task2);
    }

}
