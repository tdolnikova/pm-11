package com.dolnikova.tm.service;

import com.dolnikova.tm.api.repository.IRepository;
import com.dolnikova.tm.api.service.IService;
import com.dolnikova.tm.entity.AbstractEntity;
import com.dolnikova.tm.enumerated.DataType;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.List;

@NoArgsConstructor
public abstract class AbstractService<E extends AbstractEntity> implements IService<E> {

    @NotNull private IRepository<E> abstractRepository;

    AbstractService(@NotNull final IRepository<E> abstractRepository) {
        this.abstractRepository = abstractRepository;
    }

    @Nullable
    @Override
    public E findOneById(@Nullable final String ownerId, @Nullable final String id) {
        if (ownerId == null || ownerId.isEmpty()) return null;
        if (id == null || id.isEmpty()) return null;
        return abstractRepository.findOneById(ownerId, id);
    }

    @Override
    public E findOneByName(@Nullable final String name) {
        if (name == null || name.isEmpty()) return null;
        return abstractRepository.findOneByName(name);
    }

    @Override
    public @Nullable List<E> findAll(@Nullable final String ownerId) {
        if (ownerId == null || ownerId.isEmpty()) return null;
        return abstractRepository.findAll(ownerId);
    }

    @Override
    public @Nullable List<E> findAllByName(@Nullable final String ownerId, @Nullable final String text) {
        if (ownerId == null || ownerId.isEmpty()) return null;
        if (text == null || text.isEmpty()) return null;
        return abstractRepository.findAllByName(ownerId, text);
    }

    @Override
    public @Nullable List<E> findAllByDescription(@Nullable final String ownerId, @Nullable final String text) {
        if (ownerId == null || ownerId.isEmpty()) return null;
        if (text == null || text.isEmpty()) return null;
        return abstractRepository.findAllByDescription(ownerId, text);
    }

    @Override
    public void persist(@Nullable final String ownerId, @Nullable final E entity) {
        if (ownerId == null || ownerId.isEmpty() || entity == null) return;
        abstractRepository.persist(ownerId, entity);
    }

    @Override
    public void persistList(@Nullable String ownerId, @Nullable List<E> list) {
        if (ownerId == null || ownerId.isEmpty()) return;
        if (list == null || list.isEmpty()) return;
        abstractRepository.persistList(ownerId, list);
    }

    @Override
    public void merge(@Nullable final String ownerId,
                      @Nullable final String newData,
                      @Nullable final E entityToMerge,
                      @Nullable final DataType dataType) {
        if (ownerId == null || ownerId.isEmpty()) return;
        if (newData == null || newData.isEmpty()) return;
        if (entityToMerge == null || dataType == null) return;
        abstractRepository.merge(ownerId, newData, entityToMerge, dataType);
    }

    @Override
    public void remove(@Nullable final String ownerId, @Nullable final E entity) {
        if (ownerId == null || ownerId.isEmpty() || entity == null) return;
        abstractRepository.remove(ownerId, entity);
    }

    @Override
    public void removeAll(@Nullable final String ownerId) {
        if (ownerId == null || ownerId.isEmpty()) return;
        abstractRepository.removeAll(ownerId);
    }

    @Override
    public void saveBin(@Nullable List<E> entities) throws Exception {
        if(entities == null || entities.isEmpty()) return;
        abstractRepository.saveBin(entities);
    }

    @Override
    public void saveFasterxmlJson(@Nullable List<E> entities) throws Exception {
        if(entities == null || entities.isEmpty()) return;
        abstractRepository.saveFasterxmlJson(entities);
    }

    @Override
    public void saveFasterxmlXml(@Nullable List<E> entities) throws Exception {
        if(entities == null || entities.isEmpty()) return;
        abstractRepository.saveFasterxmlXml(entities);
    }

    @Override
    public void saveJaxbJson(@Nullable List<E> entities) throws Exception {
        if(entities == null || entities.isEmpty()) return;
        abstractRepository.saveJaxbJson(entities);
    }

    @Override
    public void saveJaxbXml(@Nullable List<E> entities) throws Exception {
        if(entities == null || entities.isEmpty()) return;
        abstractRepository.saveJaxbXml(entities);
    }

    @Override
    public @Nullable List<E> loadBin() throws Exception {
        return abstractRepository.loadBin();
    }

    @Override
    public @Nullable List<E> loadFasterxmlJson() throws Exception {
        return abstractRepository.loadFasterxmlJson();
    }

    @Override
    public @Nullable List<E> loadFasterxmlXml() throws Exception {
        return abstractRepository.loadFasterxmlXml();
    }

    @Override
    public @Nullable List<E> loadJaxbJson() throws Exception {
        return abstractRepository.loadJaxbJson();
    }

    @Override
    public @Nullable List<E> loadJaxbXml() throws Exception {
        return abstractRepository.loadJaxbXml();
    }
}
