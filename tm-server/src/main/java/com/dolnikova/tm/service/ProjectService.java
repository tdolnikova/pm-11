package com.dolnikova.tm.service;

import com.dolnikova.tm.api.repository.IProjectRepository;
import com.dolnikova.tm.api.service.IProjectService;
import com.dolnikova.tm.entity.Project;
import com.dolnikova.tm.enumerated.DataType;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.List;

@NoArgsConstructor
public final class ProjectService extends AbstractService<Project> implements IProjectService {

    private IProjectRepository projectRepository;

    public ProjectService(@NotNull final IProjectRepository projectRepository) {
        super(projectRepository);
        this.projectRepository = projectRepository;
    }

    @Nullable
    @Override
    public Project findOneById(@Nullable final String ownerId, @Nullable final String id) {
        if (ownerId == null || ownerId.isEmpty()) return null;
        if (id == null || id.isEmpty()) return null;
        return projectRepository.findOneById(ownerId, id);
    }

    @Override
    public Project findOneByName(@Nullable final String name) {
        if (name == null || name.isEmpty()) return null;
        return projectRepository.findOneByName(name);
    }

    @Override
    public @Nullable List<Project> findAll(@Nullable final String ownerId) {
        if (ownerId == null || ownerId.isEmpty()) return null;
        return projectRepository.findAll(ownerId);
    }

    @Override
    public @Nullable List<Project> findAllByName(@Nullable final String ownerId, @Nullable final String text) {
        if (ownerId == null || ownerId.isEmpty()) return null;
        if (text == null || text.isEmpty()) return null;
        return projectRepository.findAllByName(ownerId, text);
    }

    @Override
    public @Nullable List<Project> findAllByDescription(@Nullable final String ownerId, @Nullable final String text) {
        if (ownerId == null || ownerId.isEmpty()) return null;
        if (text == null || text.isEmpty()) return null;
        return projectRepository.findAllByDescription(ownerId, text);
    }

    @Override
    public void persist(@Nullable final String ownerId, @Nullable final Project entity) {
        if (ownerId == null || ownerId.isEmpty() || entity == null) return;
        projectRepository.persist(ownerId, entity);
    }

    @Override
    public void persistList(@Nullable String ownerId, @Nullable List<Project> list) {
        if (ownerId == null || ownerId.isEmpty()) return;
        if (list == null || list.isEmpty()) return;
        projectRepository.persistList(ownerId, list);
    }

    @Override
    public void merge(@Nullable final String ownerId,
                      @Nullable final String newData,
                      @Nullable final Project entityToMerge,
                      @Nullable final DataType dataType) {
        if (ownerId == null || ownerId.isEmpty()) return;
        if (newData == null || newData.isEmpty()) return;
        if (entityToMerge == null || dataType == null) return;
        projectRepository.merge(ownerId, newData, entityToMerge, dataType);
    }

    @Override
    public void remove(@Nullable final String ownerId, @Nullable final Project entity) {
        if (ownerId == null || ownerId.isEmpty() || entity == null) return;
        projectRepository.remove(ownerId, entity);
    }

    @Override
    public void removeAll(@Nullable final String ownerId) {
        if (ownerId == null || ownerId.isEmpty()) return;
        projectRepository.removeAll(ownerId);
    }

    @Override
    public void saveBin(@Nullable List<Project> entities) throws Exception {
        if(entities == null || entities.isEmpty()) return;
        projectRepository.saveBin(entities);
    }

    @Override
    public void saveFasterxmlJson(@Nullable List<Project> entities) throws Exception {
        if(entities == null || entities.isEmpty()) return;
        projectRepository.saveFasterxmlJson(entities);
    }

    @Override
    public void saveFasterxmlXml(@Nullable List<Project> entities) throws Exception {
        if(entities == null || entities.isEmpty()) return;
        projectRepository.saveFasterxmlXml(entities);
        System.out.println(this.getClass().getName());
    }

    @Override
    public void saveJaxbJson(@Nullable List<Project> entities) throws Exception {
        if(entities == null || entities.isEmpty()) return;
        projectRepository.saveJaxbJson(entities);
    }

    @Override
    public void saveJaxbXml(@Nullable List<Project> entities) throws Exception {
        if(entities == null || entities.isEmpty()) return;
        projectRepository.saveJaxbXml(entities);
    }

    @Override
    public @Nullable List<Project> loadBin() throws Exception {
        return projectRepository.loadBin();
    }

    @Override
    public @Nullable List<Project> loadFasterxmlJson() throws Exception {
        return projectRepository.loadFasterxmlJson();
    }

    @Override
    public @Nullable List<Project> loadFasterxmlXml() throws Exception {
        return projectRepository.loadFasterxmlXml();
    }

    @Override
    public @Nullable List<Project> loadJaxbJson() throws Exception {
        return projectRepository.loadJaxbJson();
    }

    @Override
    public @Nullable List<Project> loadJaxbXml() throws Exception {
        return projectRepository.loadJaxbXml();
    }

}
