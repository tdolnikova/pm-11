package com.dolnikova.tm.service;

import com.dolnikova.tm.api.repository.ITaskRepository;
import com.dolnikova.tm.api.service.ITaskService;
import com.dolnikova.tm.entity.Task;
import com.dolnikova.tm.enumerated.DataType;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.List;

@NoArgsConstructor
public final class TaskService extends AbstractService<Task> implements ITaskService {

    private ITaskRepository taskRepository;

    public TaskService(@NotNull final ITaskRepository taskRepository) {
        super(taskRepository);
        this.taskRepository = taskRepository;
    }

    @Nullable
    @Override
    public Task findOneById(@Nullable final String ownerId, @Nullable final String id) {
        if (ownerId == null || ownerId.isEmpty()) return null;
        if (id == null || id.isEmpty()) return null;
        return taskRepository.findOneById(ownerId, id);
    }

    @Override
    public Task findOneByName(@Nullable final String name) {
        if (name == null || name.isEmpty()) return null;
        return taskRepository.findOneByName(name);
    }

    @Override
    public @Nullable List<Task> findAll(@Nullable final String ownerId) {
        if (ownerId == null || ownerId.isEmpty()) return null;
        return taskRepository.findAll(ownerId);
    }

    @Override
    public @Nullable List<Task> findAllByName(@Nullable final String ownerId, @Nullable final String text) {
        if (ownerId == null || ownerId.isEmpty()) return null;
        if (text == null || text.isEmpty()) return null;
        return taskRepository.findAllByName(ownerId, text);
    }

    @Override
    public @Nullable List<Task> findAllByDescription(@Nullable final String ownerId, @Nullable final String text) {
        if (ownerId == null || ownerId.isEmpty()) return null;
        if (text == null || text.isEmpty()) return null;
        return taskRepository.findAllByDescription(ownerId, text);
    }

    @Override
    public void persist(@Nullable final String ownerId, @Nullable final Task entity) {
        if (ownerId == null || ownerId.isEmpty() || entity == null) return;
        taskRepository.persist(ownerId, entity);
    }

    @Override
    public void persistList(@Nullable String ownerId, @Nullable List<Task> list) {
        if (ownerId == null || ownerId.isEmpty()) return;
        if (list == null || list.isEmpty()) return;
        taskRepository.persistList(ownerId, list);
    }

    @Override
    public void merge(@Nullable final String ownerId,
                      @Nullable final String newData,
                      @Nullable final Task entityToMerge,
                      @Nullable final DataType dataType) {
        if (ownerId == null || ownerId.isEmpty()) return;
        if (newData == null || newData.isEmpty()) return;
        if (entityToMerge == null || dataType == null) return;
        taskRepository.merge(ownerId, newData, entityToMerge, dataType);
    }

    @Override
    public void remove(@Nullable final String ownerId, @Nullable final Task entity) {
        if (ownerId == null || ownerId.isEmpty() || entity == null) return;
        taskRepository.remove(ownerId, entity);
    }

    @Override
    public void removeAll(@Nullable final String ownerId) {
        if (ownerId == null || ownerId.isEmpty()) return;
        taskRepository.removeAll(ownerId);
    }

    @Override
    public void saveBin(@Nullable List<Task> entities) throws Exception {
        if(entities == null || entities.isEmpty()) return;
        taskRepository.saveBin(entities);
    }

    @Override
    public void saveFasterxmlJson(@Nullable List<Task> entities) throws Exception {
        if(entities == null || entities.isEmpty()) return;
        taskRepository.saveFasterxmlJson(entities);
    }

    @Override
    public void saveFasterxmlXml(@Nullable List<Task> entities) throws Exception {
        if(entities == null || entities.isEmpty()) return;
        taskRepository.saveFasterxmlXml(entities);
    }

    @Override
    public void saveJaxbJson(@Nullable List<Task> entities) throws Exception {
        if(entities == null || entities.isEmpty()) return;
        taskRepository.saveJaxbJson(entities);
    }

    @Override
    public void saveJaxbXml(@Nullable List<Task> entities) throws Exception {
        if(entities == null || entities.isEmpty()) return;
        taskRepository.saveJaxbXml(entities);
    }

    @Override
    public @Nullable List<Task> loadBin() throws Exception {
        return taskRepository.loadBin();
    }

    @Override
    public @Nullable List<Task> loadFasterxmlJson() throws Exception {
        return taskRepository.loadFasterxmlJson();
    }

    @Override
    public @Nullable List<Task> loadFasterxmlXml() throws Exception {
        return taskRepository.loadFasterxmlXml();
    }

    @Override
    public @Nullable List<Task> loadJaxbJson() throws Exception {
        return taskRepository.loadJaxbJson();
    }

    @Override
    public @Nullable List<Task> loadJaxbXml() throws Exception {
        return taskRepository.loadJaxbXml();
    }

}
