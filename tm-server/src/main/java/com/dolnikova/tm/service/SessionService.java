package com.dolnikova.tm.service;

import com.dolnikova.tm.api.repository.ISessionRepository;
import com.dolnikova.tm.api.service.ISessionService;
import com.dolnikova.tm.entity.Session;
import com.dolnikova.tm.enumerated.DataType;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.List;

@NoArgsConstructor
public final class SessionService  extends AbstractService<Session> implements ISessionService {

    private ISessionRepository sessionRepository;

    public SessionService(@NotNull final ISessionRepository sessionRepository) {
        super(sessionRepository);
        this.sessionRepository = sessionRepository;
    }

    @Nullable
    @Override
    public Session findOneById(@Nullable final String ownerId, @Nullable final String id) {
        if (ownerId == null || ownerId.isEmpty()) return null;
        if (id == null || id.isEmpty()) return null;
        return sessionRepository.findOneById(ownerId, id);
    }

    @Override
    public Session findOneByName(@Nullable final String name) {
        if (name == null || name.isEmpty()) return null;
        return sessionRepository.findOneByName(name);
    }

    @Override
    public @Nullable List<Session> findAll(@Nullable final String ownerId) {
        if (ownerId == null || ownerId.isEmpty()) return null;
        return sessionRepository.findAll(ownerId);
    }

    @Override
    public @Nullable List<Session> findAllByName(@Nullable final String ownerId, @Nullable final String text) {
        if (ownerId == null || ownerId.isEmpty()) return null;
        if (text == null || text.isEmpty()) return null;
        return sessionRepository.findAllByName(ownerId, text);
    }

    @Override
    public @Nullable List<Session> findAllByDescription(@Nullable final String ownerId, @Nullable final String text) {
        if (ownerId == null || ownerId.isEmpty()) return null;
        if (text == null || text.isEmpty()) return null;
        return sessionRepository.findAllByDescription(ownerId, text);
    }

    @Override
    public void persist(@Nullable final String ownerId, @Nullable final Session entity) {
        if (ownerId == null || ownerId.isEmpty() || entity == null) return;
        sessionRepository.persist(ownerId, entity);
    }

    @Override
    public void persistList(@Nullable String ownerId, @Nullable List<Session> list) {
        if (ownerId == null || ownerId.isEmpty()) return;
        if (list == null || list.isEmpty()) return;
        sessionRepository.persistList(ownerId, list);
    }

    @Override
    public void merge(@Nullable final String ownerId,
                      @Nullable final String newData,
                      @Nullable final Session entityToMerge,
                      @Nullable final DataType dataType) {
        if (ownerId == null || ownerId.isEmpty()) return;
        if (newData == null || newData.isEmpty()) return;
        if (entityToMerge == null || dataType == null) return;
        sessionRepository.merge(ownerId, newData, entityToMerge, dataType);
    }

    @Override
    public void remove(@Nullable final String ownerId, @Nullable final Session entity) {
        if (ownerId == null || ownerId.isEmpty() || entity == null) return;
        sessionRepository.remove(ownerId, entity);
    }

    @Override
    public void removeAll(@Nullable final String ownerId) {
        if (ownerId == null || ownerId.isEmpty()) return;
        sessionRepository.removeAll(ownerId);
    }

    @Override
    public void saveBin(@Nullable List<Session> entities) throws Exception {
        if(entities == null || entities.isEmpty()) return;
        sessionRepository.saveBin(entities);
    }

    @Override
    public void saveFasterxmlJson(@Nullable List<Session> entities) throws Exception {
        if(entities == null || entities.isEmpty()) return;
        sessionRepository.saveFasterxmlJson(entities);
    }

    @Override
    public void saveFasterxmlXml(@Nullable List<Session> entities) throws Exception {
        if(entities == null || entities.isEmpty()) return;
        sessionRepository.saveFasterxmlXml(entities);
    }

    @Override
    public void saveJaxbJson(@Nullable List<Session> entities) throws Exception {
        if(entities == null || entities.isEmpty()) return;
        sessionRepository.saveJaxbJson(entities);
    }

    @Override
    public void saveJaxbXml(@Nullable List<Session> entities) throws Exception {
        if(entities == null || entities.isEmpty()) return;
        sessionRepository.saveJaxbXml(entities);
    }

    @Override
    public @Nullable List<Session> loadBin() throws Exception {
        return sessionRepository.loadBin();
    }

    @Override
    public @Nullable List<Session> loadFasterxmlJson() throws Exception {
        return sessionRepository.loadFasterxmlJson();
    }

    @Override
    public @Nullable List<Session> loadFasterxmlXml() throws Exception {
        return sessionRepository.loadFasterxmlXml();
    }

    @Override
    public @Nullable List<Session> loadJaxbJson() throws Exception {
        return sessionRepository.loadJaxbJson();
    }

    @Override
    public @Nullable List<Session> loadJaxbXml() throws Exception {
        return sessionRepository.loadJaxbXml();
    }

}
