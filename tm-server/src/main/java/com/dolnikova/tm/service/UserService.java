package com.dolnikova.tm.service;

import com.dolnikova.tm.api.repository.IUserRepository;
import com.dolnikova.tm.api.service.IUserService;
import com.dolnikova.tm.entity.Session;
import com.dolnikova.tm.entity.User;
import com.dolnikova.tm.enumerated.DataType;
import com.dolnikova.tm.util.PasswordHashUtil;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.List;

@NoArgsConstructor
public final class UserService extends AbstractService<User> implements IUserService {

    private IUserRepository userRepository;
    private User currentUser = null;

    public UserService(@NotNull final IUserRepository taskRepository) {
        super(taskRepository);
        this.userRepository = taskRepository;
    }

    @Nullable
    @Override
    public User findOneById(@Nullable final String ownerId, @Nullable final String id) {
        if (ownerId == null || ownerId.isEmpty()) return null;
        if (id == null || id.isEmpty()) return null;
        return userRepository.findOneById(ownerId, id);
    }

    @Override
    public User findOneByName(@Nullable final String name) {
        if (name == null || name.isEmpty()) return null;
        return userRepository.findOneByName(name);
    }

    @Override
    public User findOneBySession(@Nullable Session session) {
        if (session == null) return null;
        return userRepository.findOneBySession(session);
    }

    @Override
    public @Nullable List<User> findAll(@Nullable final String ownerId) {
        if (ownerId == null || ownerId.isEmpty()) return null;
        return userRepository.findAll(ownerId);
    }

    @Override
    public @Nullable List<User> findAllByName(@Nullable final String ownerId, @Nullable final String text) {
        if (ownerId == null || ownerId.isEmpty()) return null;
        if (text == null || text.isEmpty()) return null;
        return userRepository.findAllByName(ownerId, text);
    }

    @Override
    public @Nullable List<User> findAllByDescription(@Nullable final String ownerId, @Nullable final String text) {
        if (ownerId == null || ownerId.isEmpty()) return null;
        if (text == null || text.isEmpty()) return null;
        return userRepository.findAllByDescription(ownerId, text);
    }

    @Override
    public void persist(@Nullable final String userId, @Nullable final User entity) {
        if (userId == null || userId.isEmpty()) return;
        if (entity.getDescription() == null) return;
        String hashedPassword = PasswordHashUtil.md5(entity.getDescription());
        System.out.println("Хэш. пароль: " + hashedPassword);
        entity.setDescription(hashedPassword);
        userRepository.persist(userId, entity);
    }

    @Override
    public void persistList(@Nullable String ownerId, @Nullable List<User> list) {
        if (ownerId == null || ownerId.isEmpty()) return;
        if (list == null || list.isEmpty()) return;
        userRepository.persistList(ownerId, list);
    }

    @Override
    public void merge(@Nullable final String ownerId,
                      @Nullable final String newData,
                      @Nullable final User entityToMerge,
                      @Nullable final DataType dataType) {
        if (ownerId == null || ownerId.isEmpty()) return;
        if (newData == null || newData.isEmpty()) return;
        if (entityToMerge == null || dataType == null) return;
        userRepository.merge(ownerId, newData, entityToMerge, dataType);
    }

    @Override
    public void remove(@Nullable final String ownerId, @Nullable final User entity) {
        if (ownerId == null || ownerId.isEmpty() || entity == null) return;
        userRepository.remove(ownerId, entity);
    }

    @Override
    public void removeAll(@Nullable final String ownerId) {
        if (ownerId == null || ownerId.isEmpty()) return;
        userRepository.removeAll(ownerId);
    }

    @Nullable
    public User getCurrentUser() {
        return currentUser;
    }

    public void setCurrentUser(@Nullable final User currentUser) {
        this.currentUser = currentUser;
    }

    @Override
    public void saveBin(@Nullable List<User> entities) throws Exception {
        if(entities == null || entities.isEmpty()) return;
        userRepository.saveBin(entities);
    }

    @Override
    public void saveFasterxmlJson(@Nullable List<User> entities) throws Exception {
        if(entities == null || entities.isEmpty()) return;
        userRepository.saveFasterxmlJson(entities);
    }

    @Override
    public void saveFasterxmlXml(@Nullable List<User> entities) throws Exception {
        if(entities == null || entities.isEmpty()) return;
        userRepository.saveFasterxmlXml(entities);
    }

    @Override
    public void saveJaxbJson(@Nullable List<User> entities) throws Exception {
        if(entities == null || entities.isEmpty()) return;
        userRepository.saveJaxbJson(entities);
    }

    @Override
    public void saveJaxbXml(@Nullable List<User> entities) throws Exception {
        if(entities == null || entities.isEmpty()) return;
        userRepository.saveJaxbXml(entities);
    }

    @Override
    public @Nullable List<User> loadBin() throws Exception {
        return userRepository.loadBin();
    }

    @Override
    public @Nullable List<User> loadFasterxmlJson() throws Exception {
        return userRepository.loadFasterxmlJson();
    }

    @Override
    public @Nullable List<User> loadFasterxmlXml() throws Exception {
        return userRepository.loadFasterxmlXml();
    }

    @Override
    public @Nullable List<User> loadJaxbJson() throws Exception {
        return userRepository.loadJaxbJson();
    }

    @Override
    public @Nullable List<User> loadJaxbXml() throws Exception {
        return userRepository.loadJaxbXml();
    }

}
