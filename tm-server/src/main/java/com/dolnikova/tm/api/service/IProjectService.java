package com.dolnikova.tm.api.service;

import com.dolnikova.tm.entity.Project;
import com.dolnikova.tm.enumerated.DataType;
import org.jetbrains.annotations.Nullable;

import java.util.List;

public interface IProjectService extends IService<Project> {

    @Nullable
    @Override
    Project findOneById(@Nullable final String userId, @Nullable final String id);

    @Nullable
    @Override
    Project findOneByName(final @Nullable String name);

    @Nullable
    @Override
    List<Project> findAll(@Nullable final String userId);

    @Override
    @Nullable List<Project> findAllByName(@Nullable String ownerId, @Nullable String text);

    @Override
    @Nullable List<Project> findAllByDescription(@Nullable String ownerId, @Nullable String text);

    @Override
    void persist(@Nullable final String userId, @Nullable final Project entity);

    @Override
    void persistList(final @Nullable String ownerId, final @Nullable List<Project> list);

    @Override
    void merge(@Nullable final String userId, @Nullable final String newData, @Nullable final Project entityToMerge, @Nullable final DataType dataType);

    @Override
    void remove(@Nullable final String userId, @Nullable final Project entity);

    @Override
    void removeAll(@Nullable final String userId);

    @Override
    void saveBin(final @Nullable List<Project> entities) throws Exception;

    @Override
    void saveFasterxmlJson(final @Nullable List<Project> entities) throws Exception;

    @Override
    void saveFasterxmlXml(final @Nullable List<Project> entities) throws Exception;

    @Override
    void saveJaxbJson(final @Nullable List<Project> entities) throws Exception;

    @Override
    void saveJaxbXml(final @Nullable List<Project> entities) throws Exception;

    @Override
    @Nullable List<Project> loadBin() throws Exception;

    @Override
    @Nullable List<Project> loadFasterxmlJson() throws Exception;

    @Override
    @Nullable List<Project> loadFasterxmlXml() throws Exception;

    @Override
    @Nullable List<Project> loadJaxbJson() throws Exception;

    @Override
    @Nullable List<Project> loadJaxbXml() throws Exception;
}
