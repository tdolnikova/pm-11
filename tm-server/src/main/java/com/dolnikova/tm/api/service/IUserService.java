package com.dolnikova.tm.api.service;

import com.dolnikova.tm.entity.Session;
import com.dolnikova.tm.entity.User;
import com.dolnikova.tm.enumerated.DataType;
import org.jetbrains.annotations.Nullable;

import java.util.List;

public interface IUserService extends IService<User> {

    @Nullable
    @Override
    User findOneById(final @Nullable String ownerId, final @Nullable String id);

    @Override
    User findOneByName(final @Nullable String name);

    User findOneBySession(@Nullable final Session session);

    @Override
    @Nullable List<User> findAll(final @Nullable String ownerId);

    @Override
    @Nullable List<User> findAllByName(@Nullable String ownerId, @Nullable String text);

    @Override
    @Nullable List<User> findAllByDescription(@Nullable String ownerId, @Nullable String text);

    @Override
    void persist(final @Nullable String ownerId, final @Nullable User entity);

    @Override
    void persistList(final @Nullable String ownerId, final @Nullable List<User> list);

    @Override
    void merge(final @Nullable String ownerId, final @Nullable String newData, final @Nullable User entityToMerge, final @Nullable DataType dataType);

    @Override
    void remove(final @Nullable String ownerId, final @Nullable User entity);

    @Override
    void removeAll(final @Nullable String ownerId);

    @Nullable
    User getCurrentUser();

    void setCurrentUser(@Nullable final User currentUser);

    @Override
    void saveBin(final @Nullable List<User> entities) throws Exception;

    @Override
    void saveFasterxmlJson(final @Nullable List<User> entities) throws Exception;

    @Override
    void saveFasterxmlXml(final @Nullable List<User> entities) throws Exception;

    @Override
    void saveJaxbJson(final @Nullable List<User> entities) throws Exception;

    @Override
    void saveJaxbXml(final @Nullable List<User> entities) throws Exception;

    @Override
    @Nullable List<User> loadBin() throws Exception;

    @Override
    @Nullable List<User> loadFasterxmlJson() throws Exception;

    @Override
    @Nullable List<User> loadFasterxmlXml() throws Exception;

    @Override
    @Nullable List<User> loadJaxbJson() throws Exception;

    @Override
    @Nullable List<User> loadJaxbXml() throws Exception;
}
