package com.dolnikova.tm.api;

import com.dolnikova.tm.api.service.*;
import org.jetbrains.annotations.NotNull;

public interface ServiceLocator {
    @NotNull IProjectService getProjectService();
    @NotNull ITaskService getTaskService();
    @NotNull IUserService getUserService();
    @NotNull ISessionService getSessionService();
}
