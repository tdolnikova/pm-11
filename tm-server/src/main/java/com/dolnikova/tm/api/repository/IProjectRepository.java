package com.dolnikova.tm.api.repository;

import com.dolnikova.tm.entity.Project;
import com.dolnikova.tm.enumerated.DataType;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.List;

public interface IProjectRepository extends IRepository<Project> {

    @Nullable
    @Override
    Project findOneById(final @NotNull String ownerId, final @NotNull String id);

    @Nullable
    @Override
    Project findOneByName(final @NotNull String name);

    @Override
    @Nullable List<Project> findAll(final @NotNull String ownerId);

    @Override
    @Nullable List<Project> findAllByName(@NotNull String ownerId, @NotNull String text);

    @Override
    @Nullable List<Project> findAllByDescription(@NotNull String ownerId, @NotNull String text);

    @Override
    void persist(final @NotNull String ownerId, final @NotNull Project entity);

    @Override
    void persistList(final @NotNull String ownerId, final @NotNull List<Project> list);

    @Override
    void merge(final @NotNull String ownerId, final @NotNull String newData, final @NotNull Project entityToMerge, final @NotNull DataType dataType);

    @Override
    void remove(final @NotNull String ownerId, final @NotNull Project entity);

    @Override
    void removeAll(final @NotNull String ownerId);

    @Override
    void saveBin(final @NotNull List<Project> entities) throws Exception;

    @Override
    void saveFasterxmlJson(final @NotNull List<Project> entities) throws Exception;

    @Override
    void saveFasterxmlXml(final @NotNull List<Project> entities) throws Exception;

    @Override
    void saveJaxbJson(final @NotNull List<Project> entities) throws Exception;

    @Override
    void saveJaxbXml(final @NotNull List<Project> entities) throws Exception;

    @Nullable
    @Override
    List<Project> loadBin() throws Exception;

    @Nullable
    @Override
    List<Project> loadFasterxmlJson() throws Exception;

    @Nullable
    @Override
    List<Project> loadFasterxmlXml() throws Exception;

    @Nullable
    @Override
    List<Project> loadJaxbJson() throws Exception;

    @Nullable
    @Override
    List<Project> loadJaxbXml() throws Exception;
}
