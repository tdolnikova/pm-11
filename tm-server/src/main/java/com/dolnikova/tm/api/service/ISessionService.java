package com.dolnikova.tm.api.service;

import com.dolnikova.tm.entity.Session;
import com.dolnikova.tm.enumerated.DataType;
import org.jetbrains.annotations.Nullable;

import java.util.List;

public interface ISessionService extends IService<Session> {

    @Nullable
    @Override
    Session findOneById(final @Nullable String ownerId, final @Nullable String id);

    @Nullable
    @Override
    Session findOneByName(final @Nullable String name);

    @Override
    @Nullable List<Session> findAll(final @Nullable String ownerId);

    @Override
    @Nullable List<Session> findAllByName(@Nullable String ownerId, @Nullable String text);

    @Override
    @Nullable List<Session> findAllByDescription(@Nullable String ownerId, @Nullable String text);

    @Override
    void persist(final @Nullable String ownerId, final @Nullable Session entity);

    @Override
    void persistList(final @Nullable String ownerId, final @Nullable List<Session> list);

    @Override
    void merge(final @Nullable String ownerId, final @Nullable String newData, final @Nullable Session entityToMerge, final @Nullable DataType dataType);

    @Override
    void remove(final @Nullable String ownerId, final @Nullable Session entity);

    @Override
    void removeAll(final @Nullable String ownerId);

    @Override
    void saveBin(final @Nullable List<Session> entities) throws Exception;

    @Override
    void saveFasterxmlJson(final @Nullable List<Session> entities) throws Exception;

    @Override
    void saveFasterxmlXml(final @Nullable List<Session> entities) throws Exception;

    @Override
    void saveJaxbJson(final @Nullable List<Session> entities) throws Exception;

    @Override
    void saveJaxbXml(final @Nullable List<Session> entities) throws Exception;

    @Override
    @Nullable List<Session> loadBin() throws Exception;

    @Override
    @Nullable List<Session> loadFasterxmlJson() throws Exception;

    @Override
    @Nullable List<Session> loadFasterxmlXml() throws Exception;

    @Override
    @Nullable List<Session> loadJaxbJson() throws Exception;

    @Override
    @Nullable List<Session> loadJaxbXml() throws Exception;
}
