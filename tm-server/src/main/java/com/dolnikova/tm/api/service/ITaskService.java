package com.dolnikova.tm.api.service;

import com.dolnikova.tm.entity.Task;
import com.dolnikova.tm.enumerated.DataType;
import org.jetbrains.annotations.Nullable;

import java.util.List;

public interface ITaskService extends IService<Task> {

    @Nullable
    @Override
    Task findOneById(final @Nullable String ownerId, final @Nullable String id);

    @Override
    Task findOneByName(final @Nullable String name);

    @Override
    @Nullable List<Task> findAll(final @Nullable String ownerId);

    @Override
    @Nullable List<Task> findAllByName(@Nullable String ownerId, @Nullable String text);

    @Override
    @Nullable List<Task> findAllByDescription(@Nullable String ownerId, @Nullable String text);

    @Override
    void persist(final @Nullable String ownerId, final @Nullable Task entity);

    @Override
    void persistList(final @Nullable String ownerId, final @Nullable List<Task> list);

    @Override
    void merge(final @Nullable String ownerId, final @Nullable String newData, final @Nullable Task entityToMerge, final @Nullable DataType dataType);

    @Override
    void remove(final @Nullable String ownerId, final @Nullable Task entity);

    @Override
    void removeAll(final @Nullable String ownerId);

    @Override
    void saveBin(final @Nullable List<Task> entities) throws Exception;

    @Override
    void saveFasterxmlJson(final @Nullable List<Task> entities) throws Exception;

    @Override
    void saveFasterxmlXml(final @Nullable List<Task> entities) throws Exception;

    @Override
    void saveJaxbJson(final @Nullable List<Task> entities) throws Exception;

    @Override
    void saveJaxbXml(final @Nullable List<Task> entities) throws Exception;

    @Override
    @Nullable List<Task> loadBin() throws Exception;

    @Override
    @Nullable List<Task> loadFasterxmlJson() throws Exception;

    @Override
    @Nullable List<Task> loadFasterxmlXml() throws Exception;

    @Override
    @Nullable List<Task> loadJaxbJson() throws Exception;

    @Override
    @Nullable List<Task> loadJaxbXml() throws Exception;
}
