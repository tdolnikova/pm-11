package com.dolnikova.tm.api.repository;

import com.dolnikova.tm.entity.Session;
import com.dolnikova.tm.entity.User;
import com.dolnikova.tm.enumerated.DataType;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.List;

public interface IUserRepository extends IRepository<User> {

    @Nullable
    @Override
    User findOneById(final @NotNull String ownerId, final @NotNull String id);

    @Override
    User findOneByName(final @NotNull String name);

    @Nullable
    User findOneBySession(@NotNull final Session session);

    @Override
    @Nullable List<User> findAll(final @NotNull String ownerId);

    @Override
    @Nullable List<User> findAllByName(@NotNull String ownerId, @NotNull String text);

    @Override
    @Nullable List<User> findAllByDescription(@NotNull String ownerId, @NotNull String text);

    @Override
    void persist(final @NotNull String ownerId, final @NotNull User entity);

    @Override
    void persistList(final @NotNull String ownerId, final @NotNull List<User> list);

    @Override
    void merge(final @NotNull String ownerId, final @NotNull String newData, final @NotNull User entityToMerge, final @NotNull DataType dataType);

    @Override
    void remove(final @NotNull String ownerId, final @NotNull User entity);

    @Override
    void removeAll(final @NotNull String ownerId);

    @Override
    void saveBin(final @NotNull List<User> entities) throws Exception;

    @Override
    void saveFasterxmlJson(final @NotNull List<User> entities) throws Exception;

    @Override
    void saveFasterxmlXml(final @NotNull List<User> entities) throws Exception;

    @Override
    void saveJaxbJson(final @NotNull List<User> entities) throws Exception;

    @Override
    void saveJaxbXml(final @NotNull List<User> entities) throws Exception;

    @Nullable
    @Override
    List<User> loadBin() throws Exception;

    @Nullable
    @Override
    List<User> loadFasterxmlJson() throws Exception;

    @Nullable
    @Override
    List<User> loadFasterxmlXml() throws Exception;

    @Nullable
    @Override
    List<User> loadJaxbJson() throws Exception;

    @Nullable
    @Override
    List<User> loadJaxbXml() throws Exception;
}
