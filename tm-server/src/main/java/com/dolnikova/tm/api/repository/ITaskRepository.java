package com.dolnikova.tm.api.repository;

import com.dolnikova.tm.entity.Task;
import com.dolnikova.tm.enumerated.DataType;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.List;

public interface ITaskRepository extends IRepository<Task> {

    @Nullable
    @Override
    Task findOneById(final @NotNull String ownerId, final @NotNull String id);

    @Override
    Task findOneByName(final @NotNull String name);

    @Override
    @Nullable List<Task> findAll(final @NotNull String ownerId);

    @Override
    @Nullable List<Task> findAllByName(@NotNull String ownerId, @NotNull String text);

    @Override
    @Nullable List<Task> findAllByDescription(@NotNull String ownerId, @NotNull String text);

    @Override
    void persist(final @NotNull String ownerId, final @NotNull Task entity);

    @Override
    void persistList(final @NotNull String ownerId, final @NotNull List<Task> list);

    @Override
    void merge(final @NotNull String ownerId, final @NotNull String newData, final @NotNull Task entityToMerge, final @NotNull DataType dataType);

    @Override
    void remove(final @NotNull String ownerId, final @NotNull Task entity);

    @Override
    void removeAll(final @NotNull String ownerId);

    @Override
    void saveBin(final @NotNull List<Task> entities) throws Exception;

    @Override
    void saveFasterxmlJson(final @NotNull List<Task> entities) throws Exception;

    @Override
    void saveFasterxmlXml(final @NotNull List<Task> entities) throws Exception;

    @Override
    void saveJaxbJson(final @NotNull List<Task> entities) throws Exception;

    @Override
    void saveJaxbXml(final @NotNull List<Task> entities) throws Exception;

    @Nullable
    @Override
    List<Task> loadBin() throws Exception;

    @Nullable
    @Override
    List<Task> loadFasterxmlJson() throws Exception;

    @Nullable
    @Override
    List<Task> loadFasterxmlXml() throws Exception;

    @Nullable
    @Override
    List<Task> loadJaxbJson() throws Exception;

    @Nullable
    @Override
    List<Task> loadJaxbXml() throws Exception;
}
