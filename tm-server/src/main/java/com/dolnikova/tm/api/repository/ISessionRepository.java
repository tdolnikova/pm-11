package com.dolnikova.tm.api.repository;

import com.dolnikova.tm.entity.Session;
import com.dolnikova.tm.enumerated.DataType;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.List;

public interface ISessionRepository extends IRepository<Session> {

    @Nullable
    @Override
    Session findOneById(final @NotNull String ownerId, final @NotNull String id);

    @Nullable
    @Override
    Session findOneByName(final @NotNull String name);

    @Override
    @Nullable List<Session> findAll(final @NotNull String ownerId);

    @Override
    @Nullable List<Session> findAllByName(@NotNull String ownerId, @NotNull String text);

    @Override
    @Nullable List<Session> findAllByDescription(@NotNull String ownerId, @NotNull String text);

    @Override
    void persist(final @NotNull String ownerId, final @NotNull Session entity);

    @Override
    void persistList(final @NotNull String ownerId, final @NotNull List<Session> list);

    @Override
    void merge(final @NotNull String ownerId, final @NotNull String newData, final @NotNull Session entityToMerge, final @NotNull DataType dataType);

    @Override
    void remove(final @NotNull String ownerId, final @NotNull Session entity);

    @Override
    void removeAll(final @NotNull String ownerId);

    @Override
    void saveBin(final @NotNull List<Session> entities) throws Exception;

    @Override
    void saveFasterxmlJson(final @NotNull List<Session> entities) throws Exception;

    @Override
    void saveFasterxmlXml(final @NotNull List<Session> entities) throws Exception;

    @Override
    void saveJaxbJson(final @NotNull List<Session> entities) throws Exception;

    @Override
    void saveJaxbXml(final @NotNull List<Session> entities) throws Exception;

    @Nullable
    @Override
    List<Session> loadBin() throws Exception;

    @Nullable
    @Override
    List<Session> loadFasterxmlJson() throws Exception;

    @Nullable
    @Override
    List<Session> loadFasterxmlXml() throws Exception;

    @Nullable
    @Override
    List<Session> loadJaxbJson() throws Exception;

    @Nullable
    @Override
    List<Session> loadJaxbXml() throws Exception;
}
