package com.dolnikova.tm.api.service;

import com.dolnikova.tm.entity.AbstractEntity;
import com.dolnikova.tm.enumerated.DataType;
import org.jetbrains.annotations.Nullable;

import java.util.List;

public interface IService<E extends AbstractEntity> {

    @Nullable
    E findOneById(@Nullable final String ownerId, @Nullable final String id);

    @Nullable
    E findOneByName(@Nullable final String name);

    @Nullable
    List<E> findAll(@Nullable final String ownerId);

    @Nullable
    List<E> findAllByName(@Nullable String ownerId, @Nullable String text);

    @Nullable
    List<E> findAllByDescription(@Nullable String ownerId, @Nullable String text);

    void persist(@Nullable final String ownerId, @Nullable final E entity);

    void persistList(@Nullable final String ownerId, @Nullable final List<E> list);

    void merge(@Nullable final String ownerId, @Nullable final String newData, @Nullable final E entityToMerge, @Nullable final DataType dataType);

    void remove(@Nullable final String ownerId, @Nullable final E entity);

    void removeAll(@Nullable final String ownerId);

    void saveBin(@Nullable final List<E> entities) throws Exception;

    void saveFasterxmlJson(@Nullable final List<E> entities) throws Exception;

    void saveFasterxmlXml(@Nullable final List<E> entities) throws Exception;

    void saveJaxbJson(@Nullable final List<E> entities) throws Exception;

    void saveJaxbXml(@Nullable final List<E> entities) throws Exception;

    @Nullable
    List<E> loadBin() throws Exception;

    @Nullable
    List<E> loadFasterxmlJson() throws Exception;

    @Nullable
    List<E> loadFasterxmlXml() throws Exception;

    @Nullable
    List<E> loadJaxbJson() throws Exception;

    @Nullable
    List<E> loadJaxbXml() throws Exception;

}
