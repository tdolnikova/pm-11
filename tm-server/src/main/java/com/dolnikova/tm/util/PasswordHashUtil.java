package com.dolnikova.tm.util;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.xml.bind.DatatypeConverter;
import java.security.MessageDigest;

public final class PasswordHashUtil {

    @Nullable
    public static String stringToHashString(@Nullable final String string) {
        @NotNull String hashString = "";
        try {
            @Nullable final MessageDigest md = MessageDigest.getInstance("MD5");
            md.update(string.getBytes());
            @Nullable final byte[] bytes = md.digest();
            hashString = DatatypeConverter.printHexBinary(bytes);
        } catch (Exception e) {e.printStackTrace();}
        return hashString;
    }

    @Nullable
    public static String md5(@Nullable final String md5) {
        if (md5 == null) return null;
        try {
            @NotNull final java.security.MessageDigest md =
                    java.security.MessageDigest.getInstance("MD5");
            @NotNull final byte[] array = md.digest(md5.getBytes());
            @NotNull final StringBuilder sb = new StringBuilder();
            for (int i = 0; i < array.length; ++i) {
                sb.append(Integer.toHexString((array[i] & 0xFF) | 0x100)
                        .substring(1, 3));
            }
            return sb.toString();
        } catch (java.security.NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        return null;
    }


}
